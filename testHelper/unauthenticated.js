const request = require('supertest');
const app = require('~server');

module.exports = (url, verb = 'get') => {
  describe('when user is not authenticated', () => {
    it('it behaves like unauthenticated request', async (done) => {
      const response = await request(app)[verb](url).send();

      expect(response.unauthorized).toBe(true);
      expect(response.body.message).toEqual('Please login to continue.');

      done();
    });
  });
};
