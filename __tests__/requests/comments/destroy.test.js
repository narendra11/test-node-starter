// const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

// const server = require('~server');
const {CommentFactory} = require('~factories');
const {unauthenticated, utilities, unauthorized} = require('~testHelper');

describe('API::V1::CommentsController#destroy', () => {
  let comment;
  let url = '/api/v1/comments/:id';

  beforeAll(async () => {
    comment = await CommentFactory.create();
    url = url.replace(':id', comment.id);
  });

  describe('when user is logged in', () => {
    describe('when user is not authorized', () => {
      it('gives permission error', async (done) => {
        const userToken = await utilities.token();
        unauthorized({url: url, verb: 'delete', token: userToken});
        done();
      });
    });

    describe('when user is authorized', () => {
      describe('owner can delete the comment', () => {
        // it('user or admin can only delete his own comment', async (done) => {
        //   comment = await CommentFactory.create({user: authenticationToken.user});
        //   const response = await request(server).delete(`${url}/${comment.id}`)
        //       .set('Authorization', 'Bearer ' + authenticationToken.body)
        //       .send();
        //   expect(response.noContent).toBe(true);
        //   done();
        // });
      });
    });
  });

  unauthenticated(url, 'delete');
});
