'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

const statuses = ['unseen', 'seen'];

module.exports = (sequelize, DataTypes) => {
  class Notification extends Model {
    static associate(models) {
      // define association here
      models.Notification.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });

      models.Notification.belongsTo(models.Activity, {
        foreignKey: 'activityId',
        onDelete: 'CASCADE',
        as: 'activity',
      });
    }

    static statuses() {
      return statuses;
    }

    async title() {
      const activity = await this.getActivity();
      const activityUser = await activity.getUser();
      const subject = await activity.getSubject();
      const subjectParent = await subject.parent();
      const subjectParentOwner = await subjectParent.owner();
      const subjectParentOwnerName = this.userId == subjectParentOwner.id ?
        'Your' :
        `${subjectParentOwner.fullName}'s`;

      return `${activityUser.fullName}` +
        ` has ${activity.action}` +
        ` a ${activity.subjectType}` +
        ` on ${subjectParentOwnerName}` +
        ` ${subjectParent.constructor.name}`;
    }

    async description() {
      const activity = await this.getActivity();
      const subject = await activity.getSubject();
      return subject.description();
    }
  };
  Notification.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    activityId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Activity id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Activity id is required',
        },
      },
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: statuses.indexOf('unseen'),
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Status is required',
        },
        notEmpty: {
          args: true,
          msg: 'Status is required',
        },
        isIn: [Object.keys(statuses)],
      },
      get() {
        const statusInteger = this.getDataValue('status');
        return statuses[statusInteger];
      },
      set(value) {
        const statusInteger = statuses.indexOf(value);
        if (statusInteger != -1) this.setDataValue('status', statusInteger);
      },
    },
  }, {
    sequelize,
    modelName: 'Notification',
  });
  return Notification;
};
