const {HttpStatus} = require('~helpers/constants');
const defineAbilityFor = require('~defineAbility');
const models = require('~models');
const _ = require('lodash');
const finders = require('~helpers/finders');

module.exports = (actionName, subjectName) => {
  return async (request, response, next) => {
    try {
      let authorizationSubject = subjectName;
      let resource;

      if (request.params.id) {
        resource = await finders.findInstance(models[subjectName], request.params.id);
        request[_.camelCase(subjectName)] = resource;
        authorizationSubject = resource;
      }

      if (['Comment', 'Tagging', 'Revision', 'Attachment'].includes(subjectName)) {
        if (!((subjectName == 'Comment') &&
              (actionName == 'destroy') &&
              (request.currentUser.id == request.comment.userId)
        )) {
          authorizationSubject = await parentResource(request, resource, subjectName, actionName);
        }
        if (subjectName == 'Comment') {
          request.commentable = authorizationSubject;
        } else if (subjectName == 'Tagging') {
          request.taggable = authorizationSubject;
        } else if (subjectName == 'Revision') {
          request.revision = authorizationSubject;
        } else if (subjectName == 'Attachment') {
          request.attachable = authorizationSubject;
        }
      }
      let authorizationAction = '';
      if ( (subjectName == 'Comment') &&
            (actionName == 'destroy') &&
            (request.currentUser.id == request.comment.userId)
      ) {
        authorizationAction = 'destroy';
      } else {
        authorizationAction = findAuthorizationAction(subjectName, actionName);
      }

      const ability = defineAbilityFor(request.currentUser);
      const can = ability.can(authorizationAction, authorizationSubject);
      if (!can) {
        return response.status(HttpStatus.UNAUTHORIZED).json({
          'message': 'You don\'t have access to perform this action.',
        });
      }
      next();
    } catch (err) {
      next(err);
    }
  };
};

const parentResource = async (request, resource, subjectName, actionName) => {
  let resourceType; let resourceId;

  if (subjectName == 'Comment') {
    if (resource) {
      resourceType = _.startCase(resource.commentableType);
      resourceId = resource.commentableId;
    } else {
      resourceType = request.query.commentableType || request.body.comment.commentableType;
      resourceId = request.query.commentableId || request.body.comment.commentableId;
    }
  } else if (subjectName == 'Tagging') {
    if (resource) {
      resourceType = _.startCase(resource.taggableType);
      resourceId = resource.taggableId;
    } else {
      resourceType = request.query.taggableType || request.body.tagging.taggableType;
      resourceId = request.query.taggableId || request.body.tagging.taggableId;
    }
  } else if (subjectName == 'Revision') {
    if (resource) {
      resourceType = resource.model;
      resourceId = resource.documentId;
    } else {
      resourceType = request.params.model;
      resourceId = request.params.documentId;
    }
  } else if (subjectName == 'Attachment') {
    if (resource) {
      resourceType = _.startCase(resource.attachableType);
      resourceId = resource.attachableId;
    } else {
      resourceType = request.params.attachableType || request.body.attachableType;
      resourceId = request.params.attachableId || request.body.attachableId;
    }
  }

  return await finders.findPolymorphic(resourceType, resourceId);
};

const findAuthorizationAction = (subjectName, actionName) => {
  if (subjectName == 'Comment') {
    return {
      'create': 'read',
      'index': 'read',
      'destroy': 'update',
      'update': 'update',
    }[actionName] ?? actionName;
  } else if (subjectName == 'Tagging') {
    return {
      'create': 'update',
      'index': 'read',
      'destroy': 'update',
    }[actionName] ?? actionName;
  } else if (subjectName == 'Revision') {
    return {
      'read': 'read',
      'index': 'read',
    }[actionName] ?? actionName;
  } else if (subjectName == 'Attachment') {
    return {
      'create': 'update',
      'index': 'read',
    }[actionName] ?? actionName;
  } else {
    return actionName;
  }
};
