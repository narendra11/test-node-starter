const taggingView = (tagging) => {
  return {
    id: tagging.id,
    taggableId: tagging.taggableId,
    taggableType: tagging.taggableType,
  };
};

module.exports = taggingView;
