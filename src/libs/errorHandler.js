const {HttpStatus, AuthenticationErrorMessages, ErrorMessages} = require('~helpers/constants');
require('dotenv').config();
const Sentry = require('@sentry/node');
const _ = require('lodash');

// Sentry sends errors to sentry console
Sentry.init({
  dsn: process.env.SENTRY_KEY,
  tracesSampleRate: 1.0,
});

const validationMessage = (error) => {
  let message = error.message;

  switch (error.validatorKey) {
    case 'notEmpty':
    case 'is_null':
      message = `${_.startCase(error.path)} is required`;
      break;
  }

  return message;
};


const validationErrors = (error) => {
  const errors = {};
  const messages = [];

  error.errors.forEach((e) => {
    const message = validationMessage(e);

    messages.push(message);

    if (errors.hasOwnProperty(e.path)) {
      errors[e.path].push(message);
    } else {
      errors[e.path] = [message];
    }
  });

  const fullMessage = messages.join(' / ');

  return {
    errors: errors,
    message: fullMessage,
  };
};

exceptionHandler = async (error, req, res, next) => {
  if (process.env.NODE_ENV == 'development') {
    console.log(error);
  }

  let responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
  const responseBody = {
    message: 'Internal Server Error',
  };

  if (error.message == 'RECORDS_NOT_FOUND') {
    responseStatus = HttpStatus.NOT_FOUND;
    responseBody.message = ErrorMessages.NO_RECORDS_FOUND;
    res.status(responseStatus).send(responseBody);
  }

  if (error.message == 'FILE_NOT_MATCHED') {
    responseStatus = HttpStatus.FORBIDDEN;
    responseBody.message = ErrorMessages.FILE_NOT_MATCHED;
    res.status(responseStatus).send(responseBody);
  }

  if (error.message == 'MODEL_NOT_FOUND') {
    responseStatus = HttpStatus.NOT_FOUND;
    responseBody.message = ErrorMessages.NO_MODEL_FOUND;
    res.status(responseStatus).send(responseBody);
  }

  if ((error.name == 'MulterError') && (error.message == 'File too large')) {
    responseStatus = HttpStatus.FORBIDDEN;
    responseBody.message = ErrorMessages.FILE_TOO_LARGE;
    res.status(responseStatus).send(responseBody);
  }

  // When wrong length UUID is supplied to query
  if ((error.name == 'SequelizeDatabaseError') && (error.parent.code == '22P02')) {
    responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
    responseBody.message = 'Valid Id is required';
    res.status(responseStatus).send(responseBody);
  }
  switch (error.name) {
    case 'SequelizeValidationError':
      const validationResponse = validationErrors(error);
      responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
      responseBody.errors = validationResponse.errors;
      responseBody.message = validationResponse.message;
      break;
    case 'SequelizeUniqueConstraintError':
      const uniqueValidationResponse = validationErrors(error);
      responseStatus = HttpStatus.UNPROCESSABLE_ENTITY;
      responseBody.errors = uniqueValidationResponse.errors;
      responseBody.message = uniqueValidationResponse.message;
      break;
    case 'TokenExpiredError':
      responseStatus = HttpStatus.UNAUTHORIZED;
      responseBody.message = AuthenticationErrorMessages.TOKEN_EXPIRED;
      break;
    case 'JsonWebTokenError':
      responseStatus = HttpStatus.UNAUTHORIZED;
      responseBody.message = AuthenticationErrorMessages.TOKEN_EXPIRED;
      break;
    case 'CustomError':
      responseStatus = HttpStatus.NOT_FOUND;
      responseBody.message = ErrorMessages.NO_RECORDS_FOUND;
      break;
    case 'error':
      responseBody.message = error.message || error.error;
      break;
    case 'Error':
      responseBody.message = error.message || error.error;
      break;
  }
  if (process.env.NODE_ENV == 'production') {
    Sentry.captureException(error);
  }
  res.status(responseStatus).send(responseBody);
};

module.exports = {
  exceptionHandler,
};
