const Controller = require('~controllers/v1/controller');

const {commentView, commentsIndexView} = require('~views');
const {Comment} = require('~models');

module.exports = class CommentsController extends Controller {
  static async create(request, response, next) {
    const comment = await request.commentable.createComment({
      userId: request.currentUser.id,
      body: request.body.comment.body,
    }, {user: request.currentUser});

    return response.status(Controller.HttpStatus.CREATED).json(commentView(comment));
  }

  static async update(request, response, next) {
    if (request.commentable) {
      const commentableUser = await Comment.findByPk(request.params.id);
      const comment = await commentableUser.update(request.body.comment);

      return response.status(Controller.HttpStatus.OK).json(commentView(comment));
    }
  }

  static async index(request, response, next) {
    return response.status(Controller.HttpStatus.OK)
        .json(commentsIndexView(await request.commentable.getComments()));
  }

  static async destroy(request, response, next) {
    await request.comment.destroy();

    return response.status(Controller.HttpStatus.NO_CONTENT).json();
  }
};
