const Controller = require('~controllers/v1/controller');

const {Tag} = require('~models');
const {tagView, tagIndexView} = require('~views');

module.exports = class TagsController extends Controller {
  static async create(request, response, next) {
    const tag = await Tag.create(TagsController._tagAttrs(request));

    return response.status(Controller.HttpStatus.CREATED).json(tagView(tag));
  }

  static async index(request, response, next) {
    const result = await Tag.scope('alphabetical').findAndCountAll({
      limit: request.query.limit, offset: request.offset,
    });

    return response.status(Controller.HttpStatus.OK)
        .json(tagIndexView(result.count, result.rows));
  }

  static async show(request, response, next) {
    return response.status(Controller.HttpStatus.OK).json(tagView(request.tag));
  }

  static async update(request, response, next) {
    const updatedTag = await request.tag.update(TagsController._tagAttrs(request));

    return response.status(Controller.HttpStatus.OK).json(tagView(updatedTag));
  }

  static async destroy(request, response, next) {
    await request.tag.destroy();

    return response.status(Controller.HttpStatus.NO_CONTENT).json();
  }

  static _tagAttrs(request) {
    return request.parameters.require('tag').permit('name').value();
  }
};
