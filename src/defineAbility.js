const {defineAbility} = require('@casl/ability');

// manage ==> all actions

module.exports = (user) => defineAbility((can, cannot) => {
  if (user) { // logged in
    if (user.role == 'admin') {
      can('manage', 'Tag');
      can('manage', 'Installation');
      can('manage', 'Comment');
      can('manage', 'Attachment');
    } else if (user.role == 'user') {
      can('read', 'Tag');

      can('create', 'Installation');
      can('read', 'Installation');
      can('manage', 'Installation', {userId: user.id});

      can('destroy', 'Comment', {userId: user.id});

      can('create', 'Attachment');
      can('read', 'Attachment');
      // can('manage', 'Attachment', { userId: user.id });
    }
    can('current', 'User');
    can('update', 'User');

    can('destroy', 'Session');

    can('stripeUrl', 'Oauth');
    can('facebookUrl', 'Oauth');

    can('read', 'Attachment');

    can('index', 'User');
  }

  // Even when not logged in
  can('create', 'User');
  can('emailAvailable', 'User');
  can('read', 'User');

  can('create', 'Session');

  can('sendMessage', 'ContactMessage');

  can('stripeCallback', 'Oauth');
  can('facebookCallback', 'Oauth');

  can('forgotPassword', 'Password');
  can('resetPassword', 'Password');
});
