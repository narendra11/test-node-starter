'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Attachment extends Model {
    static associate(models) {
      // define association here
      models.Attachment.belongsTo(models.Installation, {
        foreignKey: 'attachableId',
        onDelete: 'CASCADE',
        as: 'installation',
      });

      models.Attachment.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });
    }
  };
  Attachment.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    attachableType: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Attachable type is required',
        },
        notEmpty: {
          args: true,
          msg: 'Attachable type is required',
        },
      },
    },
    attachableId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Attachable id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Attachable id is required',
        },
      },
    },
    fileName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'File name is required',
        },
        notEmpty: {
          args: true,
          msg: 'File name is required',
        },
      },
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'URL is required',
        },
        notEmpty: {
          args: true,
          msg: 'URL is required',
        },
      },
    },
    contentType: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Content type is required',
        },
        notEmpty: {
          args: true,
          msg: 'Content type is required',
        },
      },
    },
    byteSize: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Byte size is required',
        },
        notEmpty: {
          args: true,
          msg: 'Byte size is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'Attachment',
  });

  return Attachment;
};
