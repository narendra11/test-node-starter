const request = require('supertest');
const _ = require('lodash');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {Tag} = require('~models');
const {TagFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');

describe('API::V1::TagsController#index', () => {
  let tags;
  const url = '/api/v1/tags';

  beforeAll(async () => {
    await Tag.destroy({where: {}, force: true});
    tags = await TagFactory.createList(15);
  });

  describe('when user is authenticated', () => {
    it('user can view all the tags', async (done) => {
      const limit = 5;
      const page = 2;
      const userToken = await utilities.token();

      const response = await request(server).get(`${url}?page=${page}&limit=${limit}`)
          .set(...userToken.header)
          .send();

      expect(response.ok).toBe(true);
      expect(response.body.total).toEqual(tags.length);
      expect(response.body.tags.length).toEqual(limit);
      expect(response.body.tags).toMatchSchema(jsonTypes.arraySchema(jsonTypes.tagSchema()));

      const orderedLimitedTags = _.sortBy(tags, (tag) => tag.name)
          .slice(((page - 1) * limit), page * limit);
      expect(response.body.tags).toEqual(orderedLimitedTags.map((tag) => jsonTypes.tag(tag)));

      done();
    });

    it('admin can view all the tags', async (done) => {
      const limit = 7;
      const page = 2;
      const adminToken = await utilities.token({role: 'admin'});

      const response = await request(server).get(`${url}?page=${page}&limit=${limit}`)
          .set(...adminToken.header)
          .send();

      expect(response.ok).toBe(true);
      expect(response.body.total).toEqual(tags.length);
      expect(response.body.tags.length).toEqual(limit);
      expect(response.body.tags).toMatchSchema(jsonTypes.arraySchema(jsonTypes.tagSchema()));

      const orderedLimitedTags = _.sortBy(tags, (tag) => tag.name)
          .slice(((page - 1) * limit), page * limit);
      expect(response.body.tags).toEqual(orderedLimitedTags.map((tag) => jsonTypes.tag(tag)));

      done();
    });
  });

  unauthenticated(url);
});
