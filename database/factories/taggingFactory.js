const Factory = require('~factories/factory');
const TagFactory = require('~factories/tagFactory');
const InstallationFactory = require('~factories/installationFactory');

module.exports = class TaggingFactory extends Factory {
  static async create(options = {}) {
    const tag = await TagFactory.create();
    const taggable = options.taggable || await InstallationFactory.create();

    return await taggable.addTag(tag);
  }
};
