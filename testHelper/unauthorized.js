const request = require('supertest');
const app = require('~server');

module.exports = async ({url, verb = 'get', token, params = {}}) => {
  const response = await request(app)[verb](url).set(...token.header).send(params);

  expect(response.unauthorized).toBe(true);
  expect(response.body.message).toEqual('You don\'t have access to perform this action.');
};
