const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {User, Installation} = require('~models');
const {InstallationFactory} = require('~factories');

describe('Installation', () => {
  let installation;
  let latestInstallation;

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    latestInstallation = await Installation.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestInstallation.id).toEqual(installation.id);
    expect(latestInstallation.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(installation.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(installation).toHaveProperty('id', 'userId', 'token');
    done();
  });

  describe('associations', () => {
    test('user', async (done) => {
      expect(await installation.getUser()).toBeInstanceOf(User);
      done();
    });

    test('comments', async (done) => {
      expect(await installation.getComments()).toBeInstanceOf(Array);
      done();
    });

    test('tags', async (done) => {
      expect(await installation.getTags()).toBeInstanceOf(Array);
      done();
    });

    test('attachments', async (done) => {
      expect(await installation.getAttachments()).toBeInstanceOf(Array);
      done();
    });
  });

  describe('validations', () => {
    it('validates user id requirement', async (done) => {
      try {
        await Installation.create({
          userId: '',
          token: installation.token,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User id is required']);
      }
      done();
    });

    it('validates token requirement', async (done) => {
      try {
        await Installation.create({
          userId: installation.userId,
          token: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Token is required']);
      }
      done();
    });
  });

  describe('methods', () => {
    test('owner', async (done) => {
      expect(await installation.owner()).toBeInstanceOf(User);
      done();
    });

    test('watchers', async (done) => {
      expect(await installation.watchers()).toBeInstanceOf(Array);
      done();
    });
  });
});
