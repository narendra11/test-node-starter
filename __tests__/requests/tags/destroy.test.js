const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {TagFactory} = require('~factories');
const {unauthenticated, unauthorized, utilities} = require('~testHelper');

describe('API::V1::TagsController#destroy', () => {
  let tag;
  let url = '/api/v1/tags/:id';

  beforeAll(async () => {
    tag = await TagFactory.create();
    url = url.replace(':id', tag.id);
  });

  describe('when user is authenticated', () => {
    describe('when user is authorized', () => {
      describe('and parameters are present', () => {
        it('admin can successfully destroys the tag', async (done) => {
          const adminToken = await utilities.token({role: 'admin'});
          const response = await request(server).delete(url).set(...adminToken.header).send();
          expect(response.noContent).toBe(true);
          done();
        });
      });
    });

    describe('when user is not authorized', () => {
      it('gives permission error', async (done) => {
        const userToken = await utilities.token();
        unauthorized({url: url, verb: 'delete', token: userToken});

        done();
      });
    });
  });

  unauthenticated(url, 'delete');
});
