module.exports = class ApplicationNotifier {
  async _emails() {
    const users = await this._users();
    return users.map((user) => user.email);
  }

  async _phoneNumbers() {
    const users = await this._users();
    return users.map((user) => user.phoneNumber).filter((phoneNumber) => phoneNumber);
  }
};
