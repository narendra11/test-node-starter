const faker = require('faker');

const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');

const {Installation} = require('~models');

module.exports = class InstallationFactory extends Factory {
  static async create(options = {}) {
    const user = options.user || await UserFactory.create();

    return await Installation.create({
      userId: user.id,
      token: faker.random.uuid(),
    }, {userId: user.id});
  }
};

