const jwt = require('jsonwebtoken');

const ALGORITHM = 'RS256';
const EXPIRY = 86400; // 24 hours

async function decode(token) {
  return await jwt.verify(token, process.env.JWT_SECRET, ALGORITHM);
}

async function encode(payload, expiresIn=EXPIRY) {
  return await jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: expiresIn}, ALGORITHM);
}

module.exports = {
  decode, encode,
};
