const faker = require('faker');

const Factory = require('~factories/factory');

module.exports = class ContactMessageFactory extends Factory {
  static async build() {
    return {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      reason: faker.lorem.words(),
      message: faker.lorem.paragraph(),
    };
  }
};

