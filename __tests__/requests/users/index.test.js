const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {User} = require('~models');
const {UserFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');

describe('API::V1::UsersController#index', () => {
  let users;
  const url = '/api/v1/users';

  beforeAll(async () => {
    await User.destroy({where: {}, force: true});
    users = await UserFactory.createList(15);
  });

  describe('when user is authenticated', () => {
    describe('when name is passed', () => {
      it('returns users list', async (done) => {
        const userToken = await utilities.token();
        const name = users[0].firstName;
        const order = 'asc';
        const response = await request(server)
            .get(`${url}?name=${name}&order=${order}`)
            .set(...userToken.header);

        expect(response.ok).toBe(true);
        expect(response.body.users)
            .toMatchSchema(jsonTypes.arraySchema(jsonTypes.userIndexSchema()));
        done();
      });
    });
  });

  unauthenticated(url);
});
