const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');
const InstallationFactory = require('~factories/installationFactory');

const {Revision} = require('~models');

module.exports = class RevisionFactory extends Factory {
  static async create(options = {}) {
    const user = options.user || await UserFactory.create();
    const installation = await InstallationFactory.create({user: user});

    return await Revision.create({
      model: installation.constructor.name,
      document: installation,
      operation: 'create',
      documentId: installation.id,
      revision: installation.revision,
    });
  }
};

