const faker = require('faker');

const Factory = require('~factories/factory');

const {SlowRequestLog} = require('~models');

module.exports = class SlowRequestLogFactory extends Factory {
  static async create() {
    const methods = ['GET', 'POST', 'PUT', 'DELETE'];
    const statuses = [200, 201, 204];
    return await SlowRequestLog.create({
      method: methods[Math.floor(Math.random() * methods.length)],
      url: faker.internet.url(),
      status: statuses[Math.floor(Math.random() * statuses.length)],
      resContentLength: faker.random.number(),
      responseTime: faker.random.float(),
      TotalTime: faker.random.float(),
    });
  }
};
