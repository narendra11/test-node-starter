const commentView = (comment) => {
  return {
    id: comment.id,
    userId: comment.userId,
    commentableId: comment.commentableId,
    commentableType: comment.commentableType,
    body: comment.body,
  };
};

module.exports = commentView;
