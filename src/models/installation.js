'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Installation extends Model {
    static associate(models) {
      // define associations here
      models.Installation.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });

      models.Installation.hasMany(models.Comment, {
        foreignKey: 'commentableId',
        constraints: false,
        scope: {
          commentableType: 'installation',
        },
        onDelete: 'CASCADE',
        as: 'comments',
      });

      models.Installation.belongsToMany(models.Tag, {
        through: {
          model: models.Tagging,
          unique: false,
          scope: {
            taggableType: 'installation',
          },
        },
        foreignKey: 'taggableId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'tags',
      });

      models.Installation.hasMany(models.Attachment, {
        foreignKey: 'attachableId',
        onDelete: 'CASCADE',
        as: 'attachments',
      });
    }

    async owner() {
      return await this.getUser();
    }

    async watchers() {
      const creator = await this.getUser();
      return [creator];
    }
  };
  Installation.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Token is required',
        },
        notEmpty: {
          args: true,
          msg: 'Token is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'Installation',
  });
  Installation.Revision = Installation.hasPaperTrail();
  return Installation;
};
