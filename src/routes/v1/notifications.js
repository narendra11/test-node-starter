const routes = require('express').Router();
const {NotificationsController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');

routes.route('/')
    .get(authentication, NotificationsController.index);

module.exports = routes;
