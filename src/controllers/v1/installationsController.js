const Controller = require('~controllers/v1/controller');

const {Installation} = require('~models');
const {installationView} = require('~views');

module.exports = class InstallationsController extends Controller {
  static async create(request, response, next) {
    const installation = await Installation.create({
      userId: request.currentUser.id,
      token: request.body.installation.token,
    }, {userId: request.currentUser.id});

    return response.status(Controller.HttpStatus.CREATED).json(installationView(installation));
  }
};
