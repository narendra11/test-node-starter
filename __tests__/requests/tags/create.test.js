const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {TagFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities, unauthorized} = require('~testHelper');

describe('API::V1::TagsController#create', () => {
  let adminToken;
  let tag;
  const url = '/api/v1/tags';

  beforeAll(async () => {
    adminToken = await utilities.token({role: 'admin'});
    tag = await TagFactory.create();
  });

  describe('when user is authenticated', () => {
    describe('when user is authorized', () => {
      describe('and parameters are missing', () => {
        it('returns validation error', async (done) => {
          const params = {
            tag: {
              name: '',
            },
          };
          const response = await request(server).post(url).set(...adminToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            name: ['Name is required'],
          });
          expect(response.body.message).toEqual('Name is required');
          done();
        });
      });

      describe('and parameters are present', () => {
        it('admin successfully creates the tag', async (done) => {
          const params = {
            tag: {
              name: tag.name + ' new',
            },
          };
          const response = await request(server).post(url).set(...adminToken.header).send(params);

          expect(response.created).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.tagSchema());

          const latestTag = await utilities.latestRecord('Tag');

          expect(response.body).toEqual(jsonTypes.tag(latestTag));
          done();
        });
      });
    });

    describe('when user is not authorized', () => {
      it('returns permission error', async (done) => {
        const params = {
          tag: {
            name: 'Funny',
          },
        };
        const userToken = await utilities.token();
        unauthorized({url: url, verb: 'post', token: userToken, params: params});
        done();
      });
    });
  });

  unauthenticated(url, 'post');
});
