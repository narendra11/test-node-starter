const Controller = require('~controllers/v1/controller');

const Stripe = require('stripe');
const queryString = require('query-string');
const axios = require('axios');

const stripe = Stripe(process.env.STRIPE_SECRET_KEY);
const redirectStripeUri = process.env.API_URL + '/oauth/stripe/callback';
const redirectFacebookUri = process.env.API_URL + '/oauth/facebook/callback';

module.exports = class OauthController extends Controller {
  static async stripeUrl(request, response, next) {
    let url = 'https://connect.stripe.com/express/oauth/authorize' +
                '?redirect_uri=' + redirectStripeUri +
                '&client_id=' + process.env.STRIPE_CLIENT_ID +
                '&state=' + request.currentUser.id +
                '&scope=read_write' +
                '&stripe_user[email]=' + request.currentUser.email;
    if (request.currentUser.phoneNumber) {
      url += '&stripe_user[phone_number]=' + request.currentUser.phoneNumber;
    }

    return response.status(Controller.HttpStatus.OK).json({url: url});
  }

  static async stripeCallback(request, response, next) {
    const stripeOauthResponse = await stripe.oauth.token({
      grant_type: 'authorization_code',
      code: request.query.code,
    });

    // gives access and refresh tokens, to be saved in DB
    // tokens can be used to get info from stripe
    return response.status(Controller.HttpStatus.OK).json(stripeOauthResponse);
  }

  static async facebookUrl(request, response, next) {
    const stringifiedParams = queryString.stringify({
      client_id: process.env.FB_APP_ID,
      redirect_uri: redirectFacebookUri,
      scope: ['email'].join(','),
      response_type: 'code',
      // auth_type: 'rerequest',
      display: 'popup',
    });

    const facebookLoginUrl = `https://www.facebook.com/v4.0/dialog/oauth?${stringifiedParams}`;

    return response.status(Controller.HttpStatus.OK).json({facebookLoginUrl: facebookLoginUrl});
  }

  static async facebookCallback(request, response, next) {
    axios({
      url: 'https://graph.facebook.com/v4.0/oauth/access_token',
      method: 'get',
      params: {
        client_id: process.env.FB_APP_ID,
        client_secret: process.env.FB_APP_SECRET,
        redirect_uri: redirectFacebookUri,
        code: request.query.code,
      },
    }).then((result) => {
      return response.status(Controller.HttpStatus.OK).json(result.data);
    }).catch((error) => {
      return response.status(error.response.status).json(error.response.data);
    });
  }
};
