const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory} = require('~factories');

describe('API::V1::PasswordController#resetPassword', () => {
  let user;
  let updatedUser;
  const url = '/api/v1/users/password';

  beforeAll(async () => {
    user = await UserFactory.create({role: 'admin'});
    updatedUser = await UserFactory.update(user.id);
  });

  describe('when user is authenticated', () => {
    it('return requirement error', async (done) => {
      const params = {
        user: {
          password: '',
          password_confirmation: '',
          reset_password_token: '',
        },
      };
      const response = await request(server).put(url).send(params);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body.errors).toEqual({
        password: ['The Password field is required.'],
        password_confirmation: ['The Password Confirmation field is required.'],
        reset_password_token: ['The Reset Password Token field is required.'],
      });
      expect(response.body.message)
          .toEqual('The Password field is required. / ' +
          'The Password Confirmation field is required. / '+
          'The Reset Password Token field is required.');
      done();
    });

    it('resets the password', async (done) => {
      const params = {
        user: {
          password: 'New Password',
          password_confirmation: 'New Password',
          reset_password_token: updatedUser.resetPasswordToken,
        },
      };
      const response = await request(server).put(url).send(params);

      expect(response.ok).toBe(true);
      expect(response.body.message).toEqual('Password updated successfully.');
      done();
    });
  });
});
