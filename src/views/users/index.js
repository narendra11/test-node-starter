const userIndexView = (users) => {
  return {
    count: users.count,
    users: users.rows.map((user) => {
      return {
        id: user.id,
        name: user.firstName + ' ' + user.lastName,
      };
    }),
  };
};

module.exports = userIndexView;
