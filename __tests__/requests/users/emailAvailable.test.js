const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require(`~server`);
const {UserFactory} = require('~factories');
const {jsonTypes} = require('~testHelper');

describe('API::V1::UsersController#emailAvailable', () => {
  const url = '/api/v1/users/email_available';

  describe('when parameters are missing', () => {
    it('returns validation error', async (done) => {
      const response = await request(server).get(url);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
      expect(response.body.errors).toEqual({
        email: ['The Email field is required.'],
      });
      expect(response.body.message).toEqual('The Email field is required.');
      done();
    });
  });

  describe('when parameters are present', () => {
    it('and email is already taken', async (done) => {
      const user = await UserFactory.create();
      const response = await request(server).get(url).query({email: user.email});

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body.message).toEqual('Email has already been taken');
      done();
    });

    it('and email is not taken', async (done) => {
      const response = await request(server).get(url).query({email: faker.internet.email()});

      expect(response.noContent).toBe(true);
      done();
    });
  });
});
