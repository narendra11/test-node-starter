const Controller = require('~controllers/v1/controller');

const {Revision} = require('~models');
const revisionView = require('~views/revisions/revision');
const revisionIndexView = require('~views/revisions/index');

module.exports = class RevisionsController extends Controller {
  static async show(request, response, next) {
    const revision = await Controller.finders.findInstance(Revision, request.params.id);

    return response.status(Controller.HttpStatus.OK).json(revisionView(revision));
  }

  static async index(request, response, next) {
    return response.status(Controller.HttpStatus.OK)
        .json(revisionIndexView(await request.revision.getRevisions()));
  }
};
