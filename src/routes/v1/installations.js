const routes = require('express').Router();
const {InstallationsController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/')
    .post(authentication, authorization('create', 'Installation'), InstallationsController.create);

module.exports = routes;
