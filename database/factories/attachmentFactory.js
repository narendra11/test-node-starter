const faker = require('faker');

const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');
const InstallationFactory = require('~factories/installationFactory');

module.exports = class AttachmentFactory extends Factory {
  static async create(options = {}) {
    const user = options.user || await UserFactory.create();
    const attachable = options.attachable || await InstallationFactory.create();

    return await attachable.createAttachment({
      userId: user.id,
      attachableType: attachable.constructor.name,
      fileName: faker.system.fileName(),
      url: faker.image.imageUrl(),
      contentType: faker.system.mimeType(),
      byteSize: faker.random.number(),
    });
  }
};

