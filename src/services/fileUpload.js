const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
require('dotenv').config();

aws.config.update({
  'secretAccessKey': process.env.AWS_SECRET_ACCESS_KEY,
  'accessKeyId': process.env.AWS_ACCESS_KEY_ID,
  'region': process.env.AWS_REGION,
});

const s3 = new aws.S3();

const upload = multer({
  limits: {
    fileSize: 1024 * 1024 * 2, // 2 MB
  },
  storage: multerS3({
    s3: s3,
    bucket: process.env.AWS_BUCKET_NAME,
    acl: 'public-read',
    metadata: function(req, file, cb) {
      if ((file.mimetype == 'image/png') ||
          (file.mimetype == 'image/jpeg') ||
          (file.mimetype == 'application/pdf')) {
        cb(null, {fieldName: 'image'});
      } else {
        cb(new Error('FILE_NOT_MATCHED'), false);
      }
    },
    key: function(req, file, cb) {
      cb(null, Date.now().toString());
    },
  }),
});

module.exports = upload;
