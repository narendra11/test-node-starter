const sequelize = require('sequelize');

const {User} = require('~models');

module.exports = class FilterUser {
  constructor(options) {
    this.name = options.name || '';
    this.order = options.order || 'asc';
    this.limit = options.limit;
  }

  get execute() {
    return this.search();
  }

  async search() {
    const users = await User.findAndCountAll({
      attributes: ['id', 'firstName', 'lastName'],
      where: {
        firstName:
          sequelize.where(
              sequelize.fn('concat',
                  (sequelize.col('firstName')),
                  ' ',
                  (sequelize.col('lastName')),
              ),
              'LIKE',
              `%${this.name}%`,
          ),
      },
      order: [
        ['createdAt', this.order],
      ],
      limit: this.limit,
    });
    return users;
  }
};
