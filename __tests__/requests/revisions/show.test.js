const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {RevisionFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');

describe('API::V1::RevisionsController#show', () => {
  let userToken;
  let revision;
  let url = '/api/v1/revisions/:id';

  beforeAll(async () => {
    userToken = await utilities.token();
    revision = await RevisionFactory.create({user: userToken.authenticationToken.user});
    url = url.replace(':id', revision.id);
  });

  describe('when user is authenticated', () => {
    it('user or admin can view revisions', async (done) => {
      const response = await request(server).get(`${url}`).set(...userToken.header).send();
      expect(response.ok).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.revisionSchema());
      done();
    });
  });

  unauthenticated(url);
});
