const userView = require('~views/users/user');

const authenticationTokenView = (authenticationToken) => {
  return {
    authToken: authenticationToken.body,
    user: userView(authenticationToken.user),
  };
};

module.exports = authenticationTokenView;
