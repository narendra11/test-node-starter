const routes = require('express').Router();
const {TagsController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/')
    .post(authentication, authorization('create', 'Tag'), TagsController.create);

routes.route('/')
    .get(authentication, authorization('read', 'Tag'), TagsController.index);

routes.route('/:id')
    .get(authentication, authorization('read', 'Tag'), TagsController.show);

routes.route('/:id')
    .put(authentication, authorization('update', 'Tag'), TagsController.update);

routes.route('/:id')
    .delete(authentication, authorization('destroy', 'Tag'), TagsController.destroy);

module.exports = routes;
