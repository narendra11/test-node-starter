const revisionView = require('~views/revisions/revision');
const revisionIndexView = (revisions) => {
  return revisions.map((revision) => revisionView(revision));
};

module.exports = revisionIndexView;
