const notificationView = require('~views/notifications/notification');

const notificationsIndexView = async (notifications) => {
  return await Promise.all(notifications.map(async (notification) =>
    await notificationView(notification)),
  );
};

module.exports = notificationsIndexView;
