const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory, CommentFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');
const {Activity} = require('~models');

describe('API::V1::NotificationController#index', () => {
  let userToken;
  const url = '/api/v1/notifications';

  beforeAll(async () => {
    userToken = await utilities.token();
  });

  describe('when user is authenticated', () => {
    describe('subject created for own post', () => {
      it('returns no notifications', async (done) => {
        await CommentFactory.create({user: userToken.authenticationToken.user});
        const response = await request(server).get(url).set(...userToken.header).send();

        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(0);
        done();
      });
    });

    describe('subject created for another post', () => {
      it('returns notifications', async (done) => {
        const comment = await CommentFactory.create({
          user: userToken.authenticationToken.user,
        });
        const newUser = await UserFactory.create();
        await Activity.create({
          userId: newUser.id,
          subjectId: comment.id,
          subjectType: 'comment',
          action: 'added',
        });

        const response = await request(server).get(url).set(...userToken.header).send();

        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(1);
        expect(response.body).toMatchSchema(jsonTypes.arraySchema(jsonTypes.notificationSchema()));
        done();
      });
    });
  });

  unauthenticated(url);
});
