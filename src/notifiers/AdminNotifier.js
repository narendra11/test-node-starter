const {User} = require('~models');
const ApplicationNotifier = require('~notifiers/ApplicationNotifier');
const mail = require('~notifiers/mail');

module.exports = class AdminNotifier extends ApplicationNotifier {
  constructor(options = {}) {
    super();
    this.options = options;
  }

  async contactMessage(contactMessage) {
    const subject = 'New contact message';
    const emails = await this._emails();

    const mailOptions = {
      to: emails,
      subject: subject,
      template: 'contactMessage',
      data: contactMessage,
    };
    mail.sendEmail(mailOptions);
  }

  async _users() {
    this.users = this.users || await User.scope('admins').findAll();
    if (!this.users.length) {
      throw new Error('No admins here');
    }

    return this.users;
  }
};
