const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {User, Activity, Comment, Installation} = require('~models');
const {UserFactory, ActivityFactory} = require('~factories');

describe('User', () => {
  let user;
  let activity;
  let latestActivity;

  beforeAll(async () => {
    user = await UserFactory.create();
    activity = await ActivityFactory.create({user: user});
    latestActivity = await Activity.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestActivity.id).toEqual(activity.id);
    expect(latestActivity.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(activity.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(user)
        .toHaveProperty('id', 'userId', 'subjectId',
            'subjectType', 'action');
    done();
  });

  describe('associations', () => {
    test('user', async (done) => {
      expect(await activity.getUser()).toBeInstanceOf(User);
      done();
    });

    test('comment', async (done) => {
      expect(await activity.getComment()).toBeInstanceOf(Comment);
      done();
    });

    test('notifications', async (done) => {
      expect(await activity.getNotifications()).toBeInstanceOf(Array);
      done();
    });
  });

  describe('validations', () => {
    it('validates user id requirement', async (done) => {
      try {
        await Activity.create({
          userId: '',
          subjectId: activity.subjectId,
          subjectType: activity.subjectType,
          action: activity.action,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User id is required']);
      }
      done();
    });

    it('validates subject id requirement', async (done) => {
      try {
        await Activity.create({
          userId: user.id,
          subjectId: '',
          subjectType: activity.subjectType,
          action: activity.action,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Subject id is required']);
      }
      done();
    });

    it('validates subject type requirement', async (done) => {
      try {
        await Activity.create({
          userId: user.id,
          subjectId: activity.subjectId,
          subjectType: '',
          action: activity.action,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Subject type is required']);
      }
      done();
    });

    it('validates action requirement', async (done) => {
      try {
        await Activity.create({
          userId: user.id,
          subjectId: activity.subjectId,
          subjectType: activity.subjectType,
          action: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Action is required']);
      }
      done();
    });
  });

  describe('methods', () => {
    test('getSubject', async (done) => {
      expect(await activity.getSubject()).toBeInstanceOf(Comment);
      done();
    });

    test('subjectParent', async (done) => {
      expect(await activity.subjectParent()).toBeInstanceOf(Installation);
      done();
    });
  });
});
