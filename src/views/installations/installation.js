const installationView = (installation) => {
  return {
    id: installation.id,
    userId: installation.userId,
    token: installation.token,
  };
};

module.exports = installationView;
