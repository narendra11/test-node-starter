const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {Attachment, Installation, User} = require('~models');
const {AttachmentFactory, InstallationFactory} = require('~factories');

describe('Attachment', () => {
  let attachment;
  let latestAttachment;

  beforeAll(async () => {
    const installation = await InstallationFactory.create();
    attachment = await AttachmentFactory.create({attachable: installation});
    latestAttachment = await Attachment.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestAttachment.id).toEqual(attachment.id);
    expect(latestAttachment.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(attachment.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(attachment)
        .toHaveProperty('id', 'userId', 'attachableType',
            'attachableId', 'fileName', 'url',
            'contentType', 'byteSize');
    done();
  });

  describe('associations', () => {
    test('installation', async (done) => {
      expect(await attachment.getInstallation()).toBeInstanceOf(Installation);
      done();
    });

    test('user', async (done) => {
      expect(await attachment.getUser()).toBeInstanceOf(User);
      done();
    });
  });

  describe('validations', () => {
    it('validates user id requirement', async (done) => {
      try {
        await Attachment.create({
          userId: '',
          attachableType: attachment.attachableType,
          attachableId: attachment.attachableId,
          fileName: attachment.fileName,
          url: attachment.url,
          contentType: attachment.contentType,
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User id is required']);
      }
      done();
    });

    it('validates attachable type requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: '',
          attachableId: attachment.attachableId,
          fileName: attachment.fileName,
          url: attachment.url,
          contentType: attachment.contentType,
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Attachable type is required']);
      }
      done();
    });

    it('validates attachable id requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: attachment.attachableType,
          attachableId: '',
          fileName: attachment.fileName,
          url: attachment.url,
          contentType: attachment.contentType,
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Attachable id is required']);
      }
      done();
    });

    it('validates file name requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: attachment.attachableType,
          attachableId: attachment.attachableId,
          fileName: '',
          url: attachment.url,
          contentType: attachment.contentType,
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['File name is required']);
      }
      done();
    });

    it('validates url requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: attachment.attachableType,
          attachableId: attachment.attachableId,
          fileName: attachment.fileName,
          url: '',
          contentType: attachment.contentType,
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['URL is required']);
      }
      done();
    });

    it('validates content type requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: attachment.attachableType,
          attachableId: attachment.attachableId,
          fileName: attachment.fileName,
          url: attachment.url,
          contentType: '',
          byteSize: attachment.byteSize,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Content type is required']);
      }
      done();
    });

    it('validates byte size requirement', async (done) => {
      try {
        await Attachment.create({
          userId: attachment.userId,
          attachableType: attachment.attachableType,
          attachableId: attachment.attachableId,
          fileName: attachment.fileName,
          url: attachment.url,
          contentType: attachment.contentType,
          byteSize: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Byte size is required']);
      }
      done();
    });
  });
});
