'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Revision extends Model {
    static associate(models) {
      // define association here
    }
  };

  Revision.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
    },
    model: {
      type: DataTypes.TEXT,
    },
    document: {
      type: DataTypes.JSONB,
    },
    operation: {
      type: DataTypes.TEXT,
    },
    documentId: {
      type: DataTypes.UUID,
    },
    revision: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'Revision',
  });

  return Revision;
};
