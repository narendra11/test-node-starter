const activityView = require('~views/activities/activity');

const notificationView = async (notification) => {
  return {
    id: notification.id,
    activity: activityView(notification.activity),
    title: await notification.title(),
    description: await notification.description(),
    status: notification.status,
  };
};

module.exports = notificationView;
