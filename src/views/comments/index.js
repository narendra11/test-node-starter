const commentView = require('~views/comments/comment');
const commentsIndexView = (comments) => {
  return comments.map((comment) => commentView(comment));
};

module.exports = commentsIndexView;
