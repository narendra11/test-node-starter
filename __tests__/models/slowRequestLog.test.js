const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {SlowRequestLog} = require('~models');
const {SlowRequestLogFactory} = require('~factories');

describe('SlowRequestLog', () => {
  let log;
  let latestLog;

  beforeAll(async () => {
    log = await SlowRequestLogFactory.create();
    latestLog = await SlowRequestLog.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestLog.id).toEqual(log.id);
    expect(latestLog.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(log.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(log).toHaveProperty('method', 'url', 'status', 'responseTime', 'totalTime');
    done();
  });

  describe('validations', () => {
    it('validates method requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: '',
          url: log.url,
          status: log.status,
          resContentLength: log.resContentLength,
          responseTime: log.responseTime,
          TotalTime: log.TotalTime,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Method is required']);
      }
      done();
    });

    it('validates url requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: log.method,
          url: '',
          status: log.status,
          resContentLength: log.resContentLength,
          responseTime: log.responseTime,
          TotalTime: log.TotalTime,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['URL is required']);
      }
      done();
    });

    it('validates status requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: log.method,
          url: log.url,
          status: '',
          resContentLength: log.resContentLength,
          responseTime: log.responseTime,
          TotalTime: log.TotalTime,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Status is required']);
      }
      done();
    });

    it('validates length requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: log.method,
          url: log.url,
          status: log.status,
          resContentLength: '',
          responseTime: log.responseTime,
          TotalTime: log.TotalTime,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Length is required']);
      }
      done();
    });

    it('validates response time requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: log.method,
          url: log.url,
          status: log.status,
          resContentLength: log.resContentLength,
          responseTime: '',
          TotalTime: log.TotalTime,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Response time is required']);
      }
      done();
    });

    it('validates total time requirement', async (done) => {
      try {
        await SlowRequestLog.create({
          method: log.method,
          url: log.url,
          status: log.status,
          resContentLength: log.resContentLength,
          responseTime: log.responseTime,
          TotalTime: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Total time is required']);
      }
      done();
    });
  });
});
