const Controller = require('~controllers/v1/controller');

const {User} = require('~models');
const {userView, userIndexView} = require('~views');
const utils = require('~helpers/utils');
const FilterUser = require('~services/filterUser');

module.exports = class UsersController extends Controller {
  static async create(request, response, next) {
    const user = await User.create(UsersController._createAttrs(request));

    return response.status(Controller.HttpStatus.CREATED).json(userView(user));
  }

  static async update(request, response, next) {
    if (('password' in request.body.user)) {
      if (!(await utils.userPasswordCheck(
          request.body.user.currentPassword,
          request.currentUser))) {
        return response.status(Controller.HttpStatus.UNAUTHORIZED)
            .json({message: 'Invalid current password'});
      }
    }

    const updatedUser = await request.currentUser.update(UsersController._updateAttrs(request));

    return response.status(Controller.HttpStatus.OK).json(userView(updatedUser));
  }

  static async current(request, response, next) {
    return response.status(Controller.HttpStatus.OK).json(userView(request.currentUser));
  }

  static async emailAvailable(request, response, next) {
    const user = await User.findOne({where: {email: request.query.email}});
    if (user) {
      return response.status(Controller.HttpStatus.UNPROCESSABLE_ENTITY)
          .json({message: 'Email has already been taken'});
    }

    return response.status(Controller.HttpStatus.NO_CONTENT).json();
  }

  static async show(request, response, next) {
    const user = await User.findOne({
      where: {slug: request.params.slug},
    });
    if (user) return response.status(Controller.HttpStatus.OK).json(userView(user));

    throw new Error('RECORDS_NOT_FOUND');
  }

  static async index(request, response, next) {
    const searchResults = new FilterUser({
      name: request.query.name,
      order: request.query.order,
      limit: request.query.limit,
    });
    const users = await searchResults.execute;

    return response.status(Controller.HttpStatus.OK).json(userIndexView(users));
  }

  static _createAttrs(request) {
    return request.parameters.require('user')
        .permit('email', 'password', 'firstName', 'lastName', 'phoneNumber')
        .value();
  }

  static _updateAttrs(request) {
    return request.parameters.require('user')
        .permit('password', 'firstName', 'lastName', 'email', 'phoneNumber')
        .value();
  }
};
