const faker = require('faker');

const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');
const InstallationFactory = require('~factories/installationFactory');

module.exports = class CommentFactory extends Factory {
  static async create(options = {}) {
    const user = options.user || await UserFactory.create();
    const commentable = options.commentable || await InstallationFactory.create();

    return await commentable.createComment({
      userId: user.id,
      body: faker.lorem.paragraph(),
    }, {user: user});
  }
};
