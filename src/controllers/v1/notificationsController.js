const Controller = require('~controllers/v1/controller');

const {notificationsIndexView} = require('~views');
const {Activity} = require('~models');

module.exports = class CommentsController extends Controller {
  static async index(request, response, next) {
    const notifications = await request.currentUser.getNotifications({
      include: {
        model: Activity,
        as: 'activity',
      },
    });

    return response.status(Controller.HttpStatus.OK)
        .json(await notificationsIndexView(notifications));
  }
};
