const routes = require('express').Router();
const {TaggingsController} = require('~controllers/v1');
const validation = require('~requests/tagging');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/')
    .post(authentication, authorization('create', 'Tagging'), TaggingsController.create);

routes.route('/')
    .get(authentication,
        authorization('read', 'Tagging'),
        validation.index,
        TaggingsController.index);

routes.route('/:id')
    .delete(authentication, authorization('destroy', 'Tagging'), TaggingsController.destroy);

module.exports = routes;
