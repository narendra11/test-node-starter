const routes = require('express').Router();

const attachments = require('~routes/v1/attachments');
const comments = require('~routes/v1/comments');
const contactMessages = require('~routes/v1/contactMessages');
const installations = require('~routes/v1/installations');
const notifications = require('~routes/v1/notifications');
const oauth = require('~routes/v1/oauth');
const revisions = require('~routes/v1/revisions');
const taggings = require('~routes/v1/taggings');
const tags = require('~routes/v1/tags');
const users = require('~routes/v1/users');


routes.get('/', (request, response) => {
  response.json({info: 'It works! Node.js, Express, and Postgres API'}).status(200);
});

routes.use('/attachments', attachments);
routes.use('/comments', comments);
routes.use('/contact_messages', contactMessages);
routes.use('/installations', installations);
routes.use('/notifications', notifications);
routes.use('/oauth', oauth);
routes.use('/revisions', revisions);
routes.use('/taggings', taggings);
routes.use('/tags', tags);
routes.use('/users', users);

module.exports = routes;
