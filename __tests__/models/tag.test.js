const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {Tag} = require('~models');
const {TagFactory} = require('~factories');

describe('Tag', () => {
  let tag;
  let latestTag;

  beforeAll(async () => {
    tag = await TagFactory.create();
    latestTag = await Tag.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestTag.id).toEqual(tag.id);
    expect(latestTag.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(tag.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(tag).toHaveProperty('name');
    done();
  });

  describe('associations', () => {
    test('installations', async (done) => {
      expect(await tag.getInstallations()).toBeInstanceOf(Array);
      done();
    });
  });

  describe('validations', () => {
    it('validates tag name requirement', async (done) => {
      try {
        await Tag.create({
          name: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Tag name is required']);
      }
      done();
    });

    it('validates tag name uniqueness', async (done) => {
      try {
        await Tag.create({
          name: tag.name,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeUniqueConstraintError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Tag Name must be unique']);
      }
      done();
    });
  });

  describe('methods', () => {
    test('getTaggables', async (done) => {
      expect(await tag.getTaggables()).toBeInstanceOf(Array);
      done();
    });
  });
});
