'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class SlowRequestLog extends Model {
    static associate(models) {
      // define association here
    }
  };
  SlowRequestLog.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    method: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: false,
          msg: 'Method is required',
        },
        notEmpty: {
          args: false,
          msg: 'Method is required',
        },
      },
    },
    url: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'URL is required',
        },
        notEmpty: {
          args: true,
          msg: 'URL is required',
        },
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Status is required',
        },
        notEmpty: {
          args: true,
          msg: 'Status is required',
        },
      },
    },
    resContentLength: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Length is required',
        },
        notEmpty: {
          args: true,
          msg: 'Length is required',
        },
      },
    },
    responseTime: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Response time is required',
        },
        notEmpty: {
          args: true,
          msg: 'Response time is required',
        },
      },
    },
    TotalTime: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Total time is required',
        },
        notEmpty: {
          args: true,
          msg: 'Total time is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'SlowRequestLog',
  });

  return SlowRequestLog;
};
