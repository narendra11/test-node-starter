const routes = require('express').Router();
const {OauthController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/stripe/url')
    .get(authentication, authorization('stripeUrl', 'Oauth'), OauthController.stripeUrl);

routes.route('/stripe/callback')
    .get(authorization('stripeCallback', 'Oauth'), OauthController.stripeCallback);

routes.route('/facebook/url')
    .get(authentication, authorization('facebookUrl', 'Oauth'), OauthController.facebookUrl);

routes.route('/facebook/callback')
    .get(authorization('facebookCallback', 'Oauth'), OauthController.facebookCallback);

module.exports = routes;
