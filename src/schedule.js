const schedule = require('node-schedule');

const job = async () => {
  await schedule.scheduleJob('m-job', '*/10 * * * *', () => {
    console.log('running node cron scheduler');
    schedule.cancelJob('m-job');
  });
};

job();
