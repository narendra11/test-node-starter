const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');
const {Installation} = require('~models');

describe('API::V1::InstallationsController#create', () => {
  let userToken;
  const url ='/api/v1/installations';

  beforeAll(async () => {
    userToken = await utilities.token();
  });

  describe('when user is authenticated', () => {
    describe('and parameters are missing', () => {
      it('returns requirement error', async (done) => {
        const params = {
          installation: {
            token: '',
          },
        };

        const response = await request(server).post(url).set(...userToken.header).send(params);

        expect(response.unprocessableEntity).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
        expect(response.body.errors).toEqual({
          token: ['Token is required'],
        });
        expect(response.body.message).toEqual('Token is required');
        done();
      });
    });

    describe('and parameters are present', () => {
      it('successfully creates the record', async (done) => {
        const params = {
          installation: {
            token: faker.random.uuid(),
          },
        };

        const response = await request(server).post(url).set(...userToken.header).send(params);
        expect(response.created).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.installationSchema());
        expect(response.body).toEqual(jsonTypes.installation(await Installation.findOne({
          order: [['createdAt', 'DESC']],
        })));
        done();
      });
    });
  });

  unauthenticated(url, 'post');
});
