const routes = require('express').Router();
const {AttachmentsController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');
const upload = require('~services/fileUpload');

routes.route('/')
    .post(authentication,
        upload.array('image'),
        authorization('create', 'Attachment'),
        AttachmentsController.create);

routes.route('/:attachableType/:attachableId')
    .get(authentication, authorization('index', 'Attachment'), AttachmentsController.index);

module.exports = routes;
