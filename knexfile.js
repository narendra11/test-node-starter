require('dotenv').config();

const {
  DB_CLIENT,
  DB_DATABASE,
  DB_USERNAME,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  TEST_DB_DATABASE,
  TEST_DB_USERNAME,
  TEST_DB_PASSWORD,
  TEST_DB_HOST,
  TEST_DB_PORT,
} = process.env;

module.exports = {
  development: {
    client: DB_CLIENT,
    connection: {
      database: DB_DATABASE,
      user: DB_USERNAME,
      password: DB_PASSWORD,
      host: DB_HOST,
      port: DB_PORT,
    },
    migrations: {
      directory: __dirname + '/src/database/migrations',
    },
    seeds: {
      directory: __dirname + '/src/database/seeds',
    },
  },

  test: {
    client: DB_CLIENT,
    connection: {
      database: TEST_DB_DATABASE,
      user: TEST_DB_USERNAME,
      password: TEST_DB_PASSWORD,
      host: TEST_DB_HOST,
      port: TEST_DB_PORT,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: __dirname + '/src/database/migrations',
    },
    seeds: {
      directory: __dirname + '/src/database/seeds',
    },
  },

  production: {
    client: DB_CLIENT,
    connection: process.env.DATABASE_URL,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      directory: __dirname + '/src/database/migrations',
    },
    seeds: {
      directory: __dirname + '/src/database/seeds',
    },
  },
};
