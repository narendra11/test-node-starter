const taggingView = require('~views/taggings/tagging');
const tagView = (tag) => {
  const tagJson = {
    id: tag.id,
    name: tag.name,
  };

  if (tag.Tagging) {
    tagJson.tagging = taggingView(tag.Tagging);
  }

  return tagJson;
};

module.exports = tagView;
