'use strict';
const {v4: uuidv4} = require('uuid');
const SequelizeSlugify = require('sequelize-slugify');
const {Model} = require('sequelize');
const utils = require('~helpers/utils');
const tokenManager = require('~libs/tokenManager');
const UserNotifier = require('~notifiers/UserNotifier');

const roles = ['admin', 'user'];

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
      models.User.hasMany(models.Installation, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'installations',
      });

      models.User.hasMany(models.AuthenticationToken, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        hooks: true,
        as: 'authenticationTokens',
      });

      models.User.hasMany(models.Comment, {
        foreignKey: 'userId',
        as: 'comments',
      });

      models.User.hasMany(models.Activity, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'activities',
      });

      models.User.hasMany(models.Notification, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'notifications',
      });

      // models.User.hasMany(models.Notification, {
      //   scope: {
      //     status: 0
      //     // status: models.Notification.statuses.indexOf('unseen')
      //   },
      //   as: 'unseenNotifications'
      // });
    }

    isAdmin() {
      return (this.role == 'admin');
    }

    static roles() {
      return roles;
    }

    async generateResetPasswordToken(user) {
      const randomCode = await utils.randomText(6);
      const resetToken = await tokenManager.encode({code: randomCode, id: user.id});
      await user.update({resetPasswordToken: resetToken, resetPasswordSentAt: new Date()});
      await new UserNotifier({users: [user]}).forgotPassword(resetToken);
    }
  };

  User.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'First name is required',
        },
        notEmpty: {
          args: true,
          msg: 'First name is required',
        },
      },
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Last name is required',
        },
        notEmpty: {
          args: true,
          msg: 'Last name is required',
        },
      },
    },
    slug: {
      type: DataTypes.STRING,
      unique: true,
    },
    fullName: {
      type: DataTypes.VIRTUAL,
      get() {
        return `${this.firstName} ${this.lastName}`;
      },
      set(value) {
        throw new Error('Do not try to set the `fullName` value!');
      },
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: 'Email format is invalid',
        },
        notNull: {
          args: true,
          msg: 'Email is required',
        },
        notEmpty: {
          args: true,
          msg: 'Email is required',
        },
      },
    },
    phoneNumber: {
      type: DataTypes.STRING,
      allowNull: true,
      set(value) {
        const normalizedPhoneNumber = utils.normalizePhoneNumber(value);
        this.setDataValue('phoneNumber', normalizedPhoneNumber);
      },
      // validate: {
      //     notNull: true,
      //     notEmpty: true
      // }
    },
    role: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: roles.indexOf('user'),
      validate: {
        notNull: true,
        notEmpty: true,
        isIn: [Object.keys(roles)],
      },
      get() {
        const roleInteger = this.getDataValue('role');
        return roles[roleInteger];
      },
      set(value) {
        const roleInteger = roles.indexOf(value);
        if (roleInteger != -1) this.setDataValue('role', roleInteger);
      },
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'active',
      validate: {
        notNull: true,
        notEmpty: true,
        isIn: {
          args: [['pending', 'active', 'inactive']],
          msg: 'Status not found',
        },
      },
    },
    resetPasswordToken: DataTypes.TEXT,
    resetPasswordSentAt: DataTypes.DATE,
    rememberCreatedAt: DataTypes.DATE,
    confirmationToken: DataTypes.TEXT,
    confirmationSentAt: DataTypes.DATE,
    confirmedAt: DataTypes.DATE,
    password: {
      type: DataTypes.VIRTUAL,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Password is required',
        },
        notEmpty: {
          args: true,
          msg: 'Password is required',
        },
      },
      set(value) {
        this.setDataValue('password', value);
        const encryptedPassword = utils.hashPassword(value);
        this.setDataValue('encryptedPassword', encryptedPassword);
      },
    },
    encryptedPassword: {
      type: DataTypes.TEXT,
    },
  }, {
    scopes: {
      admins: {where: {role: roles.indexOf('admin')}},
    },
    hooks: {
      afterCreate: (user, options) => {
        if (roles.indexOf(user.role) == 1) {
          new UserNotifier({users: [user]}).welcome();
        }
      },
    },
    sequelize,
    modelName: 'User',
  });

  SequelizeSlugify.slugifyModel(User, {
    source: ['firstName', 'lastName'],
  });
  // User.Revision = User.hasPaperTrail();
  return User;
};
