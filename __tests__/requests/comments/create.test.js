const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {InstallationFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');
const {Comment} = require('~models');

describe('API::V1::CommentsController#create', () => {
  let installation;
  let userToken;
  let params;
  const url = '/api/v1/comments';

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    userToken = await utilities.token();
    params = {
      comment: {
        commentableId: installation.id,
        commentableType: installation.constructor.name,
        body: faker.lorem.paragraph(),
      },
    };
  });

  describe('when user is authenticated', () => {
    describe('and parameters are missing', () => {
      it('returns validation error', async (done) => {
        const errorParams = {
          comment: {
            commentableType: params.comment.commentableType,
            commentableId: params.comment.commentableId,
            body: '',
          },
        };

        const response = await request(server).post(url).set(...userToken.header).send(errorParams);

        expect(response.unprocessableEntity).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
        expect(response.body.errors).toEqual({
          body: ['Body is required'],
        });
        expect(response.body.message).toEqual('Body is required');

        done();
      });
    });

    describe('and parameters are present', () => {
      it('user logged in and successfully creates the record', async (done) => {
        const response = await request(server).post(url).set(...userToken.header).send(params);

        expect(response.created).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.commentSchema());
        expect(response.body).toEqual(jsonTypes.comment(await Comment.findOne()));

        done();
      });

      it('admin logged in and successfully creates the record', async (done) => {
        const AdminToken = await utilities.token({role: 'admin'});
        const response = await request(server).post(url).set(...AdminToken.header).send(params);

        expect(response.created).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.commentSchema());
        expect(response.body).toEqual(jsonTypes.comment(await Comment.findOne()));

        done();
      });
    });
  });

  unauthenticated(url, 'post');
});
