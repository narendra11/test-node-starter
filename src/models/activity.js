'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
const _ = require('lodash');

module.exports = (sequelize, DataTypes) => {
  class Activity extends Model {
    static associate(models) {
      models.Activity.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user',
      });

      models.Activity.belongsTo(models.Comment, {
        foreignKey: 'subjectId',
        as: 'comment',
      });


      models.Activity.hasMany(models.Notification, {
        foreignKey: 'activityId',
        onDelete: 'CASCADE',
        as: 'notifications',
      });
    }

    async getSubject(options) {
      const mixinMethodName = `get${_.startCase(this.subjectType)}`;
      return await this[mixinMethodName](options);
    }

    async subjectParent() {
      const subject = await this.getSubject();
      return await subject.parent();
    }
  };

  Activity.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    subjectId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Subject id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Subject id is required',
        },
      },
    },
    subjectType: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Subject type is required',
        },
        notEmpty: {
          args: true,
          msg: 'Subject type is required',
        },
      },
    },
    action: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Action is required',
        },
        notEmpty: {
          args: true,
          msg: 'Action is required',
        },
      },
    },
  }, {
    hooks: {
      afterCreate: async (activity, options) => {
        const subject = await activity.getSubject();
        const parent = await subject.parent();
        const subjectWatchers = await subject.watchers();
        const parentWatchers = await parent.watchers();
        const watchers = [].concat(subjectWatchers, parentWatchers);
        const uniqueWatchers = _.uniqBy(watchers, 'id');
        uniqueWatchers.forEach((watcher) => {
          if (activity.userId != watcher.id) {
            activity.createNotification({
              activityId: activity.id,
              userId: watcher.id,
            });
          }
        });
      },
    },
    sequelize,
    modelName: 'Activity',
  });
  return Activity;
};
