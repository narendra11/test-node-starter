const activityView = (activity) => {
  return {
    id: activity.id,
    userId: activity.userId,
    subjectId: activity.subjectId,
    subjectType: activity.subjectType,
    action: activity.action,
  };
};

module.exports = activityView;
