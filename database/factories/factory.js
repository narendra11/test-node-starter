const _ = require('lodash');

module.exports = class Factory {
  static async createList(length, options = {}) {
    return await Promise.all(_.times(length, async (_) => await this.create(options)));
  }
};
