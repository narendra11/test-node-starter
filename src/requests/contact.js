const Validator = require('~requests/base');
const {HttpStatus} = require('~helpers/constants');
const utils = require('~helpers/utils');
const contact = async (req, res, next) => {
  const validationRule = {
    'firstName': 'required|string',
    'lastName': 'required|string',
    'email': 'required|string|email',
    'reason': 'required|string',
    'message': 'required|string|min:6',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.contactMessage, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY) .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};


module.exports = {
  contact,
};
