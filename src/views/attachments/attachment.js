const attachmentView = (attachment) => {
  return {
    id: attachment.id,
    userId: attachment.userId,
    attachableType: attachment.attachableType,
    attachableId: attachment.attachableId,
    fileName: attachment.fileName,
    url: attachment.url,
    contentType: attachment.contentType,
    byteSize: attachment.byteSize,
  };
};

module.exports = attachmentView;
