const Controller = require('~controllers/v1/controller');

const {Tag} = require('~models');
const {taggingIndexView} = require('~views');

module.exports = class TaggingsController extends Controller {
  static async create(request, response, next) {
    const tag = await Controller.finders.findInstance(Tag, request.body.tagging.tagId);
    await request.taggable.addTag(tag);
    const tags = await request.taggable.getTags();

    return response.status(Controller.HttpStatus.OK).json(taggingIndexView(tags));
  }

  static async index(request, response, next) {
    const tags = await request.taggable.getTags();

    return response.status(Controller.HttpStatus.OK).json(taggingIndexView(tags));
  }

  static async destroy(request, response, next) {
    await request.tagging.destroy();

    return response.status(Controller.HttpStatus.NO_CONTENT).json();
  }
};
