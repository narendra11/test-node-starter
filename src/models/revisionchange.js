'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class RevisionChange extends Model {
    static associate(models) {
      // define association here
    }
  };

  RevisionChange.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    model: {
      type: DataTypes.TEXT,
    },
    path: {
      type: DataTypes.TEXT,
    },
    document: {
      type: DataTypes.JSONB,
    },
    diff: {
      type: DataTypes.JSONB,
    },
    revisionId: {
      type: DataTypes.INTEGER,
    },
  }, {
    sequelize,
    modelName: 'RevisionChange',
  });

  return RevisionChange;
};
