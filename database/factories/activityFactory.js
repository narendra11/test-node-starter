const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');
const CommentFactory = require('~factories/commentFactory');

const {Activity} = require('~models');

module.exports = class ActivityFactory extends Factory {
  static async create(options = {}) {
    const user = options.user || await UserFactory.create();
    const comment = await CommentFactory.create({user: user});

    return await Activity.create({
      userId: user.id,
      subjectId: comment.id,
      subjectType: 'comment',
      action: 'added',
    });
  }
};
