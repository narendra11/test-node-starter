const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {InstallationFactory, CommentFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities, unauthorized} = require('~testHelper');
const {Comment} = require('~models');

describe('API::V1::CommentsController#update', () => {
  let installation;
  let comment;
  let params;
  let url = '/api/v1/comments/:id';

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    comment = await CommentFactory.create({
      commentable: installation,
      user: await installation.getUser(),
    });
    params = {
      comment: {
        body: faker.lorem.paragraph(),
      },
    };
    url = url.replace(':id', comment.id);
  });

  describe('when user is authenticated', () => {
    describe('when user is authorized', () => {
      describe('and parameters are present', () => {
        it('when user is authorized to update the comment', async (done) => {
          const adminToken = await utilities.token({user: await installation.getUser()});
          const response = await request(server).put(url).set(...adminToken.header).send(params);

          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.commentSchema());
          expect(response.body).toEqual(jsonTypes.comment(await Comment.findOne()));

          done();
        });
      });
    });

    describe('when user is not authorized', () => {
      it('gives permission error', async (done) => {
        const userToken = await utilities.token();
        unauthorized({url: url, verb: 'put', token: userToken, params: params});
        done();
      });
    });
  });

  unauthenticated(url, 'put');
});
