const Validator = require('~requests/base');
const {HttpStatus} = require('~helpers/constants');
const utils = require('~helpers/utils');
const index = async (req, res, next) => {
  const validationRule = {
    'taggableType': 'required',
    'taggableId': 'required',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.query, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

module.exports = {
  index,
};
