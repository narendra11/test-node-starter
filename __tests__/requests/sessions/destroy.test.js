const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {unauthenticated, utilities} = require('~testHelper');

describe('API::V1::SessionsController#destroy', () => {
  const url = '/api/v1/users/sign_out';

  describe('when user is authenticated', () => {
    describe('and parameters are present', () => {
      it('user gets successfully logged out', async (done) => {
        const userToken = await utilities.token();
        const response = await request(server).delete(`${url}`).set(...userToken.header).send();
        expect(response.ok).toBe(true);
        expect(response.body.message).toEqual('You have successfully signed out.');
        done();
      });
    });
  });

  unauthenticated(url, 'delete');
});
