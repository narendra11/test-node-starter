const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {TagFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');
const {Tag} = require('~models');

describe('API::V1::TagsController#show', () => {
  let tag;
  let latestTag;
  let url = '/api/v1/tags/:id';

  beforeAll(async () => {
    tag = await TagFactory.create();
    latestTag = await Tag.findOne({
      order: [['createdAt', 'DESC']],
    });
    url = url.replace(':id', tag.id);
  });

  describe('when user is authenticated', () => {
    describe('and parameters are present', () => {
      it('user can view the tag', async (done) => {
        const userToken = await utilities.token();
        const response = await request(server).get(`${url}`).set(...userToken.header).send();
        expect(response.ok).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.tagSchema());
        expect(response.body).toEqual(jsonTypes.tag(latestTag));
        done();
      });

      it('admin can view the tag', async (done) => {
        const adminToken = await utilities.token({role: 'admin'});
        const response = await request(server).get(`${url}`).set(...adminToken.header).send();
        expect(response.ok).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.tagSchema());
        expect(response.body).toEqual(jsonTypes.tag(latestTag));
        done();
      });
    });
  });

  unauthenticated(url);
});
