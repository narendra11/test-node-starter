const {HttpStatus, AuthenticationErrorMessages} = require('~helpers/constants');
const {AuthenticationToken, User} = require('~models');
const tokenManager = require('~libs/tokenManager');

module.exports = async (req, res, next) => {
  const authorizationHeader = req.headers.authorization || req.headers['x-user-token'];
  if (!authorizationHeader) {
    return res.status(HttpStatus.UNAUTHORIZED).send({
      message: AuthenticationErrorMessages.MISSING_ACCESS_TOKEN,
    });
  }

  let accessToken = null;
  if (authorizationHeader.indexOf('Bearer') > -1 ) {
    accessToken = authorizationHeader.split(' ')[1]; // Bearer <token>
  } else {
    accessToken = authorizationHeader;
  }

  if (!accessToken) {
    return res.status(HttpStatus.UNAUTHORIZED).send({
      message: AuthenticationErrorMessages.TOKEN_NOT_FOUND,
    });
  }
  try {
    // verify makes sure that the token hasn't expired and has been issued by us
    accessTokenPayload = await tokenManager.decode(accessToken);
    if (accessTokenPayload == null) {
      return res.status(HttpStatus.UNAUTHORIZED).send({
        message: AuthenticationErrorMessages.TOKEN_EXPIRED,
      });
    }

    if (!await AuthenticationToken.verifyAuthToken({
      body: accessToken,
      user_id: accessTokenPayload.id,
    })) {
      return res.status(HttpStatus.UNAUTHORIZED).send({
        message: AuthenticationErrorMessages.INVALID_TOKEN,
      });
    }
    // Let's pass back the decoded token to the request object
    req.currentUser = await User.findByPk(accessTokenPayload.id);
    req.currentUser.access_token = accessToken;
    // We call next to pass execution to the subsequent middleware
    next();
  } catch (err) {
    // Throw an error just in case anything goes wrong with verification
    next(err);
  }
};
