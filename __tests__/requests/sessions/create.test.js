const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory} = require('~factories');
const {jsonTypes, utilities} = require('~testHelper');

describe('API::V1::SessionsController#create', () => {
  let user;
  const url = '/api/v1/users/sign_in';

  beforeAll(async () => {
    user = await UserFactory.create();
  });

  describe('when user is authenticated', () => {
    describe('invalid details passed', () => {
      it('it behaves like unauthenticated request', async (done) => {
        const params = {
          user: {
            email: user.email,
            password: user.password + 'invalid',
          },
        };

        const response = await request(server).post(url).send(params);

        expect(response.unauthorized).toBe(true);
        expect(response.body.message).toEqual('Invalid email or password.');
        done();
      });
    });

    describe('and parameters are missing', () => {
      it('returns requirement error', async (done) => {
        const response = await request(server).post(url).send();

        expect(response.unprocessableEntity).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
        expect(response.body.errors).toEqual({
          email: ['Email is required'],
          password: ['Password is required'],
        });
        expect(response.body.message).toEqual('Email is required / Password is required');
        done();
      });
    });

    describe('and parameters are present', () => {
      it('user gets to log in', async (done) => {
        const params = {
          user: {
            email: user.email,
            password: user.password,
          },
        };

        const response = await request(server).post(url).
            set('User-Agent', faker.internet.userAgent()).
            send(params);

        expect(response.created).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.sessionSchema());

        const latestToken = await utilities.latestAuthRecord('AuthenticationToken');
        expect(response.body).toEqual(jsonTypes.session(latestToken));
        done();
      });
    });
  });
});
