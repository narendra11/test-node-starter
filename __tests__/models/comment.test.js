const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {Installation, Comment, User} = require('~models');
const {CommentFactory} = require('~factories');

describe('Comment', () => {
  let comment;
  let latestComment;

  beforeAll(async () => {
    comment = await CommentFactory.create();
    latestComment = await Comment.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestComment.id).toEqual(comment.id);
    expect(latestComment.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(comment.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(comment).toHaveProperty('id', 'userId', 'commentableId', 'commentableType', 'body');
    done();
  });

  describe('associations', () => {
    test('installation', async (done) => {
      expect(await comment.getInstallation()).toBeInstanceOf(Installation);
      done();
    });

    test('user', async (done) => {
      expect(await comment.getUser()).toBeInstanceOf(User);
      done();
    });
  });

  describe('validations', () => {
    it('validates user id requirement', async (done) => {
      try {
        await Comment.create({
          userId: '',
          commentableId: comment.commentableId,
          commentableType: comment.commentableType,
          body: comment.body,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User id is required']);
      }
      done();
    });

    it('validates commentable id requirement', async (done) => {
      try {
        await Comment.create({
          userId: comment.userId,
          commentableId: '',
          commentableType: comment.commentableType,
          body: comment.body,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Commentable id is required']);
      }
      done();
    });

    it('validates commentable type requirement', async (done) => {
      try {
        await Comment.create({
          userId: comment.userId,
          commentableId: comment.commentableId,
          commentableType: '',
          body: comment.body,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Commentable type is required']);
      }
      done();
    });

    it('validates body requirement', async (done) => {
      try {
        await Comment.create({
          userId: comment.userId,
          commentableId: comment.commentableId,
          commentableType: comment.commentableType,
          body: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Body is required']);
      }
      done();
    });
  });

  describe('methods', () => {
    test('getCommentable', async (done) => {
      expect(await comment.getCommentable()).toBeInstanceOf(Installation);
      done();
    });

    test('parent', async (done) => {
      expect(await comment.parent()).toBeInstanceOf(Installation);
      done();
    });

    test('description', async (done) => {
      expect(comment.description()).toEqual(comment.body);
      done();
    });

    test('watchers', async (done) => {
      expect(await comment.watchers()).toBeInstanceOf(Array);
      done();
    });
  });
});
