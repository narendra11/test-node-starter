const models = require('~models');
const {User, AuthenticationToken} = require('~models');

const findInstance = async (modelType, id) => {
  const model = (typeof(modelType) == 'string') ? models[modelType] : modelType;
  const modelInstance = await model.findByPk(id);
  if (!modelInstance) {
    throw new Error('RECORDS_NOT_FOUND');
  }
  return modelInstance;
};

const findPolymorphic = async (type, id) => {
  const model = await models[type];
  if (!model) {
    throw new Error('MODEL_NOT_FOUND');
  }

  return findInstance(model, id);
};

const findResetPassword = async (token) => {
  const user = await User.findOne(
      {where: {resetPasswordToken: token}},
  );
  if (!user) {
    throw new Error('RECORDS_NOT_FOUND');
  }
  return user;
};

const findUser = async (email) => {
  const user = await User.findOne({
    where: {email: email}},
  );

  if (!user) {
    throw new Error('RECORDS_NOT_FOUND');
  }
  return user;
};

const findAuth = async (token, userId) => {
  const authToken = await AuthenticationToken.findOne({
    where: {body: token, userId: userId},
  });
  if (!authToken) {
    throw new Error('RECORDS_NOT_FOUND');
  }
  return authToken;
};

module.exports = {
  findInstance, findPolymorphic, findResetPassword, findUser, findAuth,
};
