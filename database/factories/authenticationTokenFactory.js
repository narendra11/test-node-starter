const faker = require('faker');

const Factory = require('~factories/factory');
const UserFactory = require('~factories/userFactory');

const {AuthenticationToken} = require('~models');

const tokenManager = require('~libs/tokenManager');

module.exports = class AuthenticationTokenFactory extends Factory {
  static async create(options = {}) {
    const role = options.role || 'user';
    const user = options.user || await UserFactory.create({role: role});
    const token = await tokenManager.encode(user.toJSON());
    const authenticationToken = await AuthenticationToken.create({
      body: token,
      userId: user.id,
      ipAddress: faker.internet.ip(),
      userAgent: faker.internet.userAgent(),
    });
    authenticationToken.user = user;

    return authenticationToken;
  }
};
