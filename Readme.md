# Node Starter Backend

```
  Designed and Built by
   ____            _
  / ___|___  _ __ | | _____  __
 | |   / _ \| '_ \| |/ _ \ \/ /
 | |__| (_) | |_) | |  __/>  <
  \____\___/| .__/|_|\___/_/\_\
            |_|
```

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes.
See the [deployment](#deployment) section for notes on how to deploy the project
on a live system.

### Prerequisites

To run the Node application, you will need to have the following software installed:
- nvm (node version manager to run different versions of node.js)
- node (node.js javascript runtime)

#### NodeJs

Version v10 or above

##### NodeJs Installation Guides

- [macOS] / [Windows] / [Ubuntu] (https://nodejs.org/en/download/)

#### PostgreSQL

We recommend installing version 9.6 or higher.

When setting up PostgreSQL for the first time, you will need to create a new
user. A commonly used username is `postgres`.

Take note of the username and password as they will be needed to configure the
Node js app. See the [database config](#database-config) section for details.


### Setup

Ensure you have installed all [prerequisites](#prerequisites) above and then:

1. Clone the repository
2. `cd` to project directory
3. Configure [environment variables](#environment-variables)
4. Configure [database](#database-config)
5. Run `make install` to install dependencies
6. Run `npm run migrate` to set up dev/test databases
7. Run `npm run dev` to start local server
8. Visit http://localhost:3005/api/v1 (base URL of API)

#### Environment variables
To setup application environment variables, copy and rename
`.env.example` to `.env`. (This file is added to
gitignore.)

#### Database Config
Open [`.env`](.env) to see an
example database configuration.

If the example configuration does not work as is, it's likely that you need to
specify the username and password of your PostgreSQL user.
In the `development` and `test` sections, set the `username` and `password`
values to match your user.

Login to postgres as root user

Create postgres user (as in .env file)
`CREATE USER coplex WITH ENCRYPTED PASSWORD 'password';`

Create database
`CREATE DATABASE coplex_starter_express;`
`CREATE DATABASE coplex_starter_express_test;`

Grant privileges
`GRANT ALL PRIVILEGES ON DATABASE coplex_starter_express TO coplex;`
`GRANT ALL PRIVILEGES ON DATABASE coplex_starter_express_test TO coplex;`

## Starting Server

To start the server on the default port of 3005, run the following:

`npm run dev`

## API documentation
Check live documentation at following URL
https://documenter.getpostman.com/view/11714162/TVYAfLak

Postman collection
https://gitlab.com/narendra11/test-node-starter/uploads/91afaa3401b62f015067c677111de78a/coplex_starter_backend.postman_collection.json

## Admin
1. Setup admin repository https://gitlab.com/narendra11/test-node-starter-admin
2. Start server with `npm start`
3. open url https://app.forestadmin.com/projects
4. login with
    username ==> narendra@coplex.com
    password ==> Teche@20!7
5. choose coplex_admin section
6. Re-type your password for data access

# Sentry
Sentry's application monitoring platform helps every developer diagnose, fix, and 
optimize the performance of their code.
Credentials:
  username: narendra@coplex.com
  password: Teche@20!7
