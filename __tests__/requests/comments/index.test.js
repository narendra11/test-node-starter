const request = require('supertest');
const _ = require('lodash');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {CommentFactory, InstallationFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');

describe('API::V1::CommentsController#index', () => {
  let comments; let installation;
  let commentableTypeName;
  const url = '/api/v1/comments';

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    comments = await CommentFactory.createList(5, {commentable: installation});
    await CommentFactory.createList(2);
    commentableTypeName = installation.constructor.name;
  });

  describe('when user is authenticated', () => {
    describe('and parameters are present', () => {
      it('user can view the comments', async (done) => {
        const userToken = await utilities.token();
        const response = await request(server)
            .get(`${url}?commentableType=${commentableTypeName}&commentableId=${installation.id}`)
            .set(...userToken.header)
            .send();

        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(comments.length);
        expect(response.body).toMatchSchema(jsonTypes.arraySchema(jsonTypes.commentSchema()));

        const orderedComments = _.sortBy(comments, (comment) => comment.createdAt).reverse();
        expect(response.body).toEqual(orderedComments.map((comment) => jsonTypes.comment(comment)));
        done();
      });

      it('admin can view the comments', async (done) => {
        const adminToken = await utilities.token({role: 'admin'});

        const response = await request(server)
            .get(`${url}?commentableType=${commentableTypeName}&commentableId=${installation.id}`)
            .set(...adminToken.header)
            .send();

        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(comments.length);
        expect(response.body).toMatchSchema(jsonTypes.arraySchema(jsonTypes.commentSchema()));

        const orderedComments = _.sortBy(comments, (comment) => comment.createdAt).reverse();
        expect(response.body).toEqual(orderedComments.map((comment) => jsonTypes.comment(comment)));
        done();
      });
    });
  });

  unauthenticated(url);
});
