const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {TagFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities, unauthorized} = require('~testHelper');

describe('API::V1::TagsController#update', () => {
  let tag;
  let url = '/api/v1/tags/:id';

  beforeAll(async () => {
    tag = await TagFactory.create();
    url = url.replace(':id', tag.id);
  });

  describe('when user is authenticated', () => {
    describe('when user is authorized', () => {
      describe('and parameters are present', () => {
        it('admin successfully updates the tag', async (done) => {
          const adminToken = await utilities.token({role: 'admin'});
          const newParams = {
            tag: {
              name: tag.name + ' update',
            },
          };
          const response = await request(server).put(`${url}`)
              .set(...adminToken.header)
              .send(newParams);
          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.tagSchema());
          const latestTag = await utilities.latestRecord('Tag');
          expect(response.body).toEqual(jsonTypes.tag(latestTag));
          done();
        });
      });
    });

    describe('when user is not authorized', () => {
      it('gives permission error', async (done) => {
        const userToken = await utilities.token();
        const newParams = {
          tag: {
            name: tag.name + ' update',
          },
        };
        unauthorized({url: url, verb: 'put', token: userToken, params: newParams});
        done();
      });
    });
  });

  unauthenticated(url, 'put');
});
