'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Tag extends Model {
    static associate(models) {
      // define association here
      models.Tag.belongsToMany(models.Installation, {
        through: {
          model: models.Tagging,
          unique: false,
        },
        foreignKey: 'tagId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'installations',
      });
    }

    async getTaggables(options) {
      return await this.getInstallations(options);
    }
  };
  Tag.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    name: {
      type: DataTypes.STRING,
      unique: {
        args: true,
        msg: 'Tag Name must be unique',
      },
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Tag name is required',
        },
        notEmpty: {
          args: true,
          msg: 'Tag name is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'Tag',
    scopes: {
      alphabetical: {
        order: [['name', 'ASC']],
      },
    },
  });

  return Tag;
};
