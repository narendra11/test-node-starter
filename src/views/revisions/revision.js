const _ = require('lodash');
const views = require('~views');

const revisionView = (revision) => {
  const viewName = _.camelCase(revision.model) + 'View';
  return {
    id: revision.id,
    model: revision.model,
    operation: revision.operation,
    documentId: revision.documentId,
    revision: revision.revision,
    document: views[viewName](revision.document),
  };
};

module.exports = revisionView;
