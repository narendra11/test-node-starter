const AttachmentsController = require('~controllers/v1/attachmentsController');
const CommentsController = require('~controllers/v1/commentsController');
const ContactMessagesController = require('~controllers/v1/contactMessagesController');
const InstallationsController = require('~controllers/v1/installationsController');
const NotificationsController = require('~controllers/v1/notificationsController');
const OauthController = require('~controllers/v1/oauthController');
const PasswordController = require('~controllers/v1/passwordController');
const RevisionsController = require('~controllers/v1/revisionsController');
const SessionsController = require('~controllers/v1/sessionsController');
const TaggingsController = require('~controllers/v1/taggingsController');
const TagsController = require('~controllers/v1/tagsController');
const UsersController = require('~controllers/v1/usersController');

module.exports = {
  AttachmentsController,
  CommentsController,
  ContactMessagesController,
  InstallationsController,
  NotificationsController,
  OauthController,
  PasswordController,
  RevisionsController,
  SessionsController,
  TaggingsController,
  TagsController,
  UsersController,
};
