const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {InstallationFactory, TaggingFactory} = require('~factories');
const {unauthenticated, utilities} = require('~testHelper');

describe('API::V1::TaggingsController#index', () => {
  let installation;
  let taggings;
  let taggableTypeName;
  const url = '/api/v1/taggings';

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    taggings = await TaggingFactory.createList(5, {taggable: installation});
    taggableTypeName = installation.constructor.name;
  });

  describe('when user is authenticated', () => {
    describe('and parameters are present', () => {
      it('if user is authorized to create the tagging', async (done) => {
        const userToken = await utilities.token();
        const response = await request(server)
            .get(`${url}?taggableType=${taggableTypeName}&taggableId=${installation.id}`)
            .set(...userToken.header)
            .send();

        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(taggings.length);
        done();
      });

      it('admin is authorized to create the tagging', async (done) => {
        const adminToken = await utilities.token({role: 'admin'});
        const response = await request(server)
            .get(`${url}?taggableType=${taggableTypeName}&taggableId=${installation.id}`)
            .set(...adminToken.header)
            .send();
        expect(response.ok).toBe(true);
        expect(response.body.length).toEqual(taggings.length);
        done();
      });
    });
  });

  unauthenticated(url, 'post');
});
