'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
const expiresIn = 3600;

module.exports = (sequelize, DataTypes) => {
  class AuthenticationToken extends Model {
    static associate(models) {
      // define association here
      models.AuthenticationToken.belongsTo(models.User, {
        foreignKey: 'userId',
        onDelete: 'CASCADE',
        as: 'user',
      });
    };
    static verifyAuthToken(params) {
      const authToken = AuthenticationToken.findOne(
          {where: {id: params.user_id, body: params.body}},
      );
      if (authToken) {
        return true;
      }
      return false;
    };
  };
  AuthenticationToken.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Body is required',
        },
        notEmpty: {
          args: true,
          msg: 'Body is required',
        },
      },
    },
    ipAddress: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'IP address is required',
        },
        notEmpty: {
          args: true,
          msg: 'IP address is required',
        },
      },
    },
    userAgent: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User agent is required',
        },
        notEmpty: {
          args: true,
          msg: 'User agent is required',
        },
      },
    },
    expiresIn: {
      type: DataTypes.INTEGER,
      defaultValue: expiresIn,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Expires in is required',
        },
        notEmpty: {
          args: true,
          msg: 'Expires in is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'AuthenticationToken',
  });

  return AuthenticationToken;
};
