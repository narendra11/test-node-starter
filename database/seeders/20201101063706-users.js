'use strict';
const utils = require('~helpers/utils');
const {v4: uuidv4} = require('uuid');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const id = uuidv4();
    await queryInterface.bulkInsert('Users', [{
      id: id,
      firstName: 'Admin',
      lastName: 'User',
      email: 'admin@example.com',
      phoneNumber: '+19100000000',
      role: 0,
      status: 'active',
      encryptedPassword: await utils.hashPassword('password'),
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Users', [{
      email: 'admin@example.com',
    }]);
  },
};
