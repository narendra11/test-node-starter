const routes = require('express').Router();
const {CommentsController} = require('~controllers/v1');
const validation = require('~requests/comment');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/')
    .post(authentication, authorization('create', 'Comment'),
        validation.comment,
        CommentsController.create);

routes.route('/')
    .get(authentication, authorization('index', 'Comment'),
        validation.index,
        CommentsController.index);

routes.route('/:id')
    .put(authentication, authorization('update', 'Comment'), CommentsController.update);

routes.route('/:id')
    .delete(authentication, authorization('destroy', 'Comment'), CommentsController.destroy);

module.exports = routes;
