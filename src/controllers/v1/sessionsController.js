const Controller = require('~controllers/v1/controller');

const {UserMessage} = require('~helpers/constants');
const {User, AuthenticationToken} = require('~models');
const {authenticationTokenView} = require('~views');
const tokenManager = require('~libs/tokenManager');
const utils = require('~helpers/utils');

module.exports = class SessionsController extends Controller {
  static async create(request, response, next) {
    const user = await Controller.finders.findUser(request.body.user.email);

    if (user && await utils.userPasswordCheck(request.body.user.password, user)) {
      const authenticationToken = await AuthenticationToken.create({
        body: await tokenManager.encode(user.toJSON()),
        userId: user.id,
        ipAddress: request.headers['x-forwarded-for'] || request.connection.remoteAddress,
        userAgent: request.get('user-agent'),
      });

      await authenticationToken.reload({
        include: {
          model: User,
          as: 'user',
        },
      });

      return response.status(Controller.HttpStatus.CREATED)
          .json(authenticationTokenView(authenticationToken));
    } else {
      return response.status(Controller.HttpStatus.UNAUTHORIZED)
          .json({message: UserMessage.INVALID_LOGIN});
    }
  }

  static async destroy(request, response, next) {
    const authToken = await Controller.finders.findAuth(
        (request.headers['x-user-token'] || request.currentUser.access_token),
        request.currentUser.id,
    );
    await authToken.destroy();

    return response.status(Controller.HttpStatus.OK)
        .json({message: UserMessage.LOGOUT});
  }
};
