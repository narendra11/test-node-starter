const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {TaggingFactory} = require('~factories');
const {unauthenticated, utilities, unauthorized} = require('~testHelper');

describe('API::V1::TaggingsController#destroy', () => {
  let tagging;
  let url = '/api/v1/taggings/:id';

  beforeAll(async () => {
    tagging = await TaggingFactory.create();
  });

  describe('when user is authenticated', () => {
    describe('and parameters are present', () => {
      describe('when user is authorized', () => {
        it('admin is authorized to delete the tagging', async (done) => {
          const adminToken = await utilities.token({role: 'admin'});
          const taggingAdmin = await TaggingFactory.create({
            user: adminToken.authenticationToken.user,
          });
          const urlAdmin = url.replace(':id', taggingAdmin[0].id);
          const response = await request(server).delete(`${urlAdmin}`)
              .set(...adminToken.header)
              .send();
          expect(response.noContent).toBe(true);
          done();
        });
      });

      describe('when user is not authorized', () => {
        it('returns permission error', async (done) => {
          const userToken = await utilities.token();
          url = url.replace(':id', tagging[0].id);
          unauthorized({url: url, verb: 'delete', token: userToken});
          done();
        });
      });
    });
  });

  unauthenticated(url, 'delete');
});
