const nodemailer = require('nodemailer');
const fs = require('fs');
require('dotenv').config();

const readFile = async (path) => {
  return await fs.promises.readFile(path, {encoding: 'utf-8'});
};

const transportOnBrowser = nodemailer.createTransport({
  jsonTransport: true,
});

const transporter = nodemailer.createTransport({
  service: 'sendgrid',
  auth: {
    user: process.env.SENDGRID_USERNAME,
    pass: process.env.SENDGRID_API_KEY,
  },
});

module.exports = {
  readFile, transportOnBrowser, transporter,
};
