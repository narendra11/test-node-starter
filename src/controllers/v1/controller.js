require('dotenv').config();
const {HttpStatus} = require('~helpers/constants');
const finders = require('~helpers/finders');

module.exports = class Controller {
  static HttpStatus = HttpStatus;
  static finders = finders;
};
