const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory} = require('~factories');
const {jsonTypes} = require('~testHelper');

describe('API::V1::PasswordController#forgotPassword', () => {
  let user;
  const url = '/api/v1/users/password';

  beforeAll(async () => {
    user = await UserFactory.create();
  });

  describe('when user is authenticated', () => {
    describe('and parameters are missing', () => {
      it('returns requirement error', async (done) => {
        const response = await request(server).post(url).send();

        expect(response.unprocessableEntity).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
        expect(response.body.errors).toEqual({
          email: ['The Email field is required.'],
        });
        expect(response.body.message).toEqual('The Email field is required.');
        done();
      });
    });

    describe('and parameters are present', () => {
      it('sends the mail', async (done) => {
        const params = {
          user: {
            email: user.email,
          },
        };

        const response = await request(server).post(url).send(params);

        expect(response.ok).toBe(true);
        expect(response.body.message)
            .toEqual('Forgot password reset link sent your registered email.');
        done();
      });
    });
  });
});
