const bcrypt = require('bcryptjs');
const SALT_ROUNDS = 10;

/**
 * encrypt password.
 * @param {string} password  plain text password string.
 * @return {string} return encrypted password .
 */
const hashPassword = (password) => {
  if (password != null) {
    return bcrypt.hashSync(password, SALT_ROUNDS);
  }
  return '';
};

/**
 * compare password string to encrypt hash.
 * @param {string} password  plain text password string.
 * @param {string} hash  encrypted hash.
 * @return {boolean} return status .
 */

const userPasswordCheck = async (password, user) => {
  return await bcrypt.compare(password, user.encryptedPassword);
};

/**
 * generate random string.
 * @param {number} len  plain text password string.
 * @return {string} return random string .
 */
const randomText = async (len) => {
  const set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
  const setLen = set.length;
  let salt = '';
  for (let i = 0; i < len; i++) {
    const p = Math.floor(Math.random() * setLen);
    salt += set[p];
  }
  return salt;
};

/**
 * Create response of validation error message
 * @param {object} error validation error object .
 * @return {string} return filtered object .
 */

const errorResponse = (errors) => {
  const error = {};
  error.message = '';
  error.errors = errors;
  const messages = [];
  Object.keys(errors).forEach(function(key) {
    messages.push(errors[key]);
  });
  error.message = messages.join(' / ');
  return error;
};

const normalizePhoneNumber = (phoneNumber) => {
  // Remove any characters except digits and +
  let formattedPhoneNumber = phoneNumber.replace(/[^\d+]+/g, '');

  // Replace leading 00 with +
  formattedPhoneNumber = formattedPhoneNumber.replace(/^00/, '+');

  // if the string starts with 1, add a plus
  if (formattedPhoneNumber.match(/^1/)) formattedPhoneNumber = '+' + formattedPhoneNumber;

  // if the string is not empty and does not start with +, add +1
  if (formattedPhoneNumber && !formattedPhoneNumber.match(/^\+/)) {
    formattedPhoneNumber = '+1' + formattedPhoneNumber;
  }

  // If number starts with + then it remains unchanged to support international numbers
  // If no country code is present then +1 is added by default

  return formattedPhoneNumber;
};

module.exports = {
  hashPassword, userPasswordCheck, randomText, errorResponse, normalizePhoneNumber,
};
