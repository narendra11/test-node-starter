const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {Tagging, Tag} = require('~models');
const {TaggingFactory} = require('~factories');

describe('Tagging', () => {
  let tagging;
  let latestTagging;

  beforeAll(async () => {
    tagging = await TaggingFactory.create();
    latestTagging = await Tagging.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestTagging.id).toEqual(tagging[0].id);
    expect(latestTagging.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(tagging[0].id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(tagging[0]).toHaveProperty('id', 'tagId', 'taggableId', 'taggableType');
    done();
  });

  describe('associations', () => {
    test('tag', async (done) => {
      expect(await tagging[0].getTag()).toBeInstanceOf(Tag);
      done();
    });
  });

  describe('validations', () => {
    it('validates tagId requirement', async (done) => {
      try {
        await Tagging.create({
          tagId: '',
          taggableId: tagging[0].taggableId,
          taggableType: tagging[0].taggableType,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Tag id is required']);
      }
      done();
    });

    it('validates taggableId requirement', async (done) => {
      try {
        await Tagging.create({
          tagId: tagging[0].tagId,
          taggableId: '',
          taggableType: tagging[0].taggableType,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Taggable id is required']);
      }
      done();
    });

    it('validates taggableType requirement', async (done) => {
      try {
        await Tagging.create({
          tagId: tagging[0].tagId,
          taggableId: tagging[0].taggableId,
          taggableType: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Taggable type is required']);
      }
      done();
    });
  });
});
