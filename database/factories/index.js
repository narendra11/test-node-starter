const ActivityFactory = require('~factories/activityFactory');
const AttachmentFactory = require('~factories/attachmentFactory');
const AuthenticationTokenFactory = require('~factories/authenticationTokenFactory');
const CommentFactory = require('~factories/commentFactory');
const ContactMessageFactory = require('~factories/contactMessageFactory');
const InstallationFactory = require('~factories/installationFactory');
const RevisionFactory = require('~factories/revisionFactory');
const SlowRequestLogFactory = require('~factories/slowRequestLogFactory');
const TagFactory = require('~factories/tagFactory');
const TaggingFactory = require('~factories/taggingFactory');
const UserFactory = require('~factories/userFactory');

module.exports = {
  ActivityFactory,
  AttachmentFactory,
  AuthenticationTokenFactory,
  CommentFactory,
  ContactMessageFactory,
  InstallationFactory,
  RevisionFactory,
  SlowRequestLogFactory,
  TagFactory,
  TaggingFactory,
  UserFactory,
};
