const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory, ContactMessageFactory} = require('~factories');
const {jsonTypes} = require('~testHelper');
const {User} = require('~models');

describe('API::V1::ContactMessagesController#sendMessage', () => {
  let contactMessage;
  const url = '/api/v1/contact_messages';

  beforeAll(async () => {
    contactMessage = await ContactMessageFactory.build();
  });

  describe('and invalid parameters have passed', () => {
    it('invalid email', async (done) => {
      const params = {
        contact_message: {
          first_name: contactMessage.firstName,
          last_name: contactMessage.lastName,
          email: contactMessage.firstName,
          reason: contactMessage.reason,
          message: contactMessage.message,
        },
      };
      const response = await request(server).post(url).send(params);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
      expect(response.body.message).toEqual('The Email format is invalid.');
      done();
    });
  });

  describe('and parameters are missing', () => {
    it('error in sending mail', async (done) => {
      const params = {
        contact_message: {},
      };

      const response = await request(server).post(url).send(params);
      expect(response.unprocessableEntity).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
      expect(response.body.message).toEqual('First Name is required' +
        ' / Last Name is required' +
        ' / Email is required' +
        ' / Reason is required' +
        ' / Message is required');
      done();
    });
  });

  describe('and parameters are present', () => {
    it('but admin is not found', async (done) => {
      await User.scope('admins').destroy({force: true});
      const params = {
        contact_message: contactMessage,
      };
      const response = await request(server).post(url).send(params);
      expect(response.notFound).toBe(true);
      expect(response.body.message).toEqual('Unable to to send message.');
      done();
    });

    it('successfully sends the mail', async (done) => {
      await UserFactory.create({role: 'admin'});
      const params = {
        contact_message: contactMessage,
      };

      const response = await request(server).post(url).send(params);
      expect(response.ok).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.contactSchema());
      expect(response.body.message).toEqual('Sent contact message');
      done();
    });
  });
});
