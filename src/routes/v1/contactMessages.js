const routes = require('express').Router();
const {ContactMessagesController} = require('~controllers/v1');
const validation = require('~requests/contact');
const authorization = require('~middlewares/authorization');

routes.route('/')
    .post(validation.contact, authorization('sendMessage', 'ContactMessage'),
        ContactMessagesController.sendMessage);

module.exports = routes;
