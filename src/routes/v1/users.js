const routes = require('express').Router();
const {UsersController, SessionsController, PasswordController} = require('~controllers/v1');
const validation = require('~requests/user');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/sign_in')
    .post(authorization('create', 'Session'), validation.signIn, SessionsController.create);

routes.route('/sign_out')
    .delete(authentication, authorization('destroy', 'Session'), SessionsController.destroy);

routes.route('/current_user')
    .get(authentication, authorization('current', 'User'), UsersController.current);

routes.route('/')
    .put(authentication,
        validation.update,
        authorization('update', 'User'),
        UsersController.update);

routes.route('/email_available')
    .get(authorization('emailAvailable', 'User'),
        validation.emailAvailable,
        UsersController.emailAvailable);

routes.route('/password')
    .post(validation.forgotPassword,
        authorization('forgotPassword', 'Password'),
        PasswordController.forgotPassword)
    .put(validation.resetPassword,
        authorization('resetPassword', 'Password'),
        PasswordController.resetPassword);

routes.route('/')
    .post(authorization('create', 'User'), UsersController.create);

routes.route('/show/:slug')
    .get(authorization('read', 'User'), UsersController.show);

routes.route('/')
    .get(authentication, authorization('index', 'User'), UsersController.index);

module.exports = routes;
