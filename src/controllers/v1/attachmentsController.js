const Controller = require('~controllers/v1/controller');

const {attachmentIndexView} = require('~views');

module.exports = class AttachmentsController extends Controller {
  static async create(request, response, next) {
    if (request.files.length <= 0) {
      return response.status(Controller.HttpStatus.UNPROCESSABLE_ENTITY).json({
        message: 'Please upload atleast 1 file.',
      });
    }

    const attachable = await Controller.finders.findPolymorphic(
        request.body.attachableType,
        request.body.attachableId,
    );

    const uploads = [];
    for (const file of request.files) {
      const uploadedFile = await attachable.createAttachment({
        userId: request.currentUser.id,
        attachableType: request.body.attachableType,
        fileName: file.originalname,
        url: file.location,
        contentType: file.mimetype,
        byteSize: file.size,
      });
      uploads.push(uploadedFile);
    };

    return response.status(Controller.HttpStatus.CREATED).json(attachmentIndexView(uploads));
  }

  static async index(request, response, next) {
    return response.status(Controller.HttpStatus.OK).json(attachmentIndexView(
        await request.attachable.getAttachments(),
    ));
  }
};
