const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {User} = require('~models');
const {UserFactory} = require('~factories');

describe('User', () => {
  let user;
  let latestUser;

  beforeAll(async () => {
    user = await UserFactory.create();
    latestUser = await User.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestUser.id).toEqual(user.id);
    expect(latestUser.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(user.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(user)
        .toHaveProperty('id', 'firstName', 'lastName',
            'email', 'phoneNumber', 'role',
            'status', 'slug');
    done();
  });

  describe('associations', () => {
    test('installations', async (done) => {
      expect(await user.getInstallations()).toBeInstanceOf(Array);
      done();
    });

    test('authenticationTokens', async (done) => {
      expect(await user.getAuthenticationTokens()).toBeInstanceOf(Array);
      done();
    });

    test('comments', async (done) => {
      expect(await user.getComments()).toBeInstanceOf(Array);
      done();
    });

    test('activities', async (done) => {
      expect(await user.getActivities()).toBeInstanceOf(Array);
      done();
    });

    test('notifications', async (done) => {
      expect(await user.getNotifications()).toBeInstanceOf(Array);
      done();
    });
  });

  describe('default properties', () => {
    it('validates role', async (done) => {
      expect(user.role).toEqual('user');
      done();
    });

    it('validates status', async (done) => {
      expect(user.status).toEqual('active');
      done();
    });

    it('validates slug', async (done) => {
      expect(user.slug).toEqual(user.firstName.toLowerCase() + '-' + user.lastName.toLowerCase());
      done();
    });

    it('validates fullName', async (done) => {
      expect(user.fullName).toEqual(user.firstName + ' ' + user.lastName);
      done();
    });
  });

  describe('validations', () => {
    it('validates first name requirement', async (done) => {
      try {
        await User.create({
          firstName: '',
          lastName: user.lastName,
          email: user.email + 'firstName',
          password: 'password',
          phoneNumber: user.phoneNumber,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['First name is required']);
      }
      done();
    });

    it('validates last name requirement', async (done) => {
      try {
        await User.create({
          firstName: user.firstName,
          lastName: '',
          email: user.email + 'lastName',
          password: 'password',
          phoneNumber: user.phoneNumber,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Last name is required']);
      }
      done();
    });

    it('validates email requirement', async (done) => {
      try {
        await User.create({
          firstName: user.firstName,
          lastName: user.lastName,
          email: '',
          password: 'password',
          phoneNumber: user.phoneNumber,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(2);
        expect(error.errors.map((err) => err.message))
            .toEqual(['Email format is invalid', 'Email is required']);
      }
      done();
    });

    it('validates password requirement', async (done) => {
      try {
        await User.create({
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email + 'password',
          password: '',
          phoneNumber: user.phoneNumber,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Password is required']);
      }
      done();
    });

    it('validates email format', async (done) => {
      try {
        await User.create({
          firstName: user.firstName,
          lastName: user.lastName,
          email: 'invalid email',
          password: 'password',
          phoneNumber: user.phoneNumber,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Email format is invalid']);
      }
      done();
    });
  });

  describe('methods', () => {
    test('roles', async (done) => {
      expect(User.roles()).toEqual(['admin', 'user']);
      done();
    });
  });
});
