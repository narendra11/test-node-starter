require('dotenv').config();
const ApplicationNotifier = require('~notifiers/ApplicationNotifier');
const mail = require('~notifiers/mail');
const twilio = require('~libs/twilio');

module.exports = class UserNotifier extends ApplicationNotifier {
  constructor(options = {}) {
    super();
    this.options = options;
  }

  async forgotPassword(resetToken) {
    const subject = 'Reset password';
    const emails = this._emails();
    const data = {
      firstName: this._users()[0].firstName,
      resetPasswordToken: resetToken,
      webServer: process.env.WEB_SERVER,
    };

    const mailOptions = {
      to: emails,
      subject: subject,
      template: 'forgotPassword',
      data: data,
    };
    mail.sendEmail(mailOptions);
  }

  async sendTextMessage(message) {
    this._phoneNumbers().forEach((phoneNumber) => {
      twilio.sendSMS({
        phoneNumber: phoneNumber,
        message: message,
      });
    });
  }

  async welcome() {
    const message = `Thank You for registering with ${process.env.APP_NAME}.`;
    this.sendTextMessage(message);
  }

  async goodBye() {
    const message = `Bye Bye.`;
    this.sendTextMessage(message);
  }

  _users() {
    return this.options.users;
  }

  _phoneNumbers() {
    const users = this._users();
    return users.map((user) => user.phoneNumber).filter((phoneNumber) => phoneNumber);
  }
};
