const tagView = require('~views/tags/tag');
const tagsIndexView = (count, tags) => {
  return {
    total: count,
    tags: tags.map((tag) => tagView(tag)),
  };
};

module.exports = tagsIndexView;
