const faker = require('faker');

const Factory = require('~factories/factory');

const {User} = require('~models');

const utils = require('~helpers/utils');
const tokenManager = require('~libs/tokenManager');

module.exports = class UserFactory extends Factory {
  static async create(options = {}) {
    return await User.create({
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: 'testPassword',
      phoneNumber: faker.phone.phoneNumber('##########'),
      role: (options.role) ? 'admin' : 'user',
    });
  }


  static async update(id) {
    const user = await User.findByPk(id);
    const randomCode = await utils.randomText(6);
    const resetToken = await tokenManager.encode({code: randomCode, id: id});

    return await user.update({
      resetPasswordToken: resetToken,
      resetPasswordSentAt: new Date(),
    });
  }
};
