const Controller = require('~controllers/v1/controller');
const {User} = require('~models');
const AdminNotifier = require('~notifiers/AdminNotifier');

module.exports = class ContactMessagesController extends Controller {
  static async sendMessage(request, response, next) {
    const adminsCount = await User.scope('admins').count();
    if (adminsCount == 0) {
      return response.status(Controller.HttpStatus.NOT_FOUND).json({
        message: 'Unable to to send message.',
      });
    }
    await new AdminNotifier().contactMessage(request.body.contactMessage);

    return response.status(Controller.HttpStatus.OK).json({message: 'Sent contact message'});
  }
};
