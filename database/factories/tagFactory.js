const faker = require('faker');

const Factory = require('~factories/factory');

const {Tag} = require('~models');

module.exports = class TagFactory extends Factory {
  static async create() {
    return await Tag.create({
      name: faker.lorem.words(),
    });
  }
};
