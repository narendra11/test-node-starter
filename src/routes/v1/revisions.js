const routes = require('express').Router();
const {RevisionsController} = require('~controllers/v1');
const authentication = require('~middlewares/authentication');
const authorization = require('~middlewares/authorization');

routes.route('/:id')
    .get(authentication, authorization('read', 'Revision'), RevisionsController.show);

routes.route('/:model/:documentId')
    .get(authentication, authorization('index', 'Revision'), RevisionsController.index);

module.exports = routes;
