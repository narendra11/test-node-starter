const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory} = require('~factories');
const {jsonTypes} = require('~testHelper');

describe('API::V1::UsersController#show', () => {
  let url = '/api/v1/users/show/:slug';

  beforeAll(async () => {
    user = await UserFactory.create();
    url = url.replace(':slug', user.slug);
  });

  describe('when slug is correct', () => {
    it('returns user details', async (done) => {
      const response = await request(server).get(url).send();

      expect(response.ok).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.userSchema());
      expect(response.body).toEqual(jsonTypes.user(user));
      done();
    });
  });
});
