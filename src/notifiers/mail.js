require('dotenv').config();
const previewEmail = require('preview-email');
const path = require('path');
const handlebars = require('handlebars');
const nodeMailerUtil = require('~notifiers/nodeMailerUtil');

const sendEmail = async (options) => {
  const mailOptions = options;
  mailOptions.subject = `${process.env.APP_NAME} | ${mailOptions.subject}`;
  mailOptions.from = process.env.FROM_MAIL;
  mailOptions.to = Array.isArray(options.to) ? options.to.join(', ') : options.to;

  const templatePath = await path.join(__dirname, `../views/mailers/${mailOptions.template}.html`);
  const rawTemplate = await nodeMailerUtil.readFile(templatePath);
  const compiledTemplate = await handlebars.compile(rawTemplate);
  mailOptions.html = compiledTemplate(mailOptions.data);

  if (process.env.NODE_ENV !== 'test') {
    if (process.env.NODE_ENV == 'development') { // mail not send and automatically opens on browser
      const mailResponse = await nodeMailerUtil.transportOnBrowser.sendMail(mailOptions);
      previewEmail(JSON.parse(mailResponse.message));
    } else { // mail is sent
      await nodeMailerUtil.transporter.sendMail(options);
    }
  }
};

module.exports = {
  sendEmail,
};
