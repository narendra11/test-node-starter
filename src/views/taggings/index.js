const tagView = require('~views/tags/tag');
const taggingsIndexView = (tags) => {
  return tags.map((tag) => tagView(tag));
};

module.exports = taggingsIndexView;
