const request = require('supertest');
const faker = require('faker');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {UserFactory} = require('~factories');
const {jsonTypes, utilities} = require('~testHelper');

describe('API::V1::UsersController#create', () => {
  let user;
  const url = '/api/v1/users';

  beforeAll(async () => {
    user = await UserFactory.create();
  });

  describe('when parameters are missing', () => {
    it('returns validation error', async (done) => {
      const params = {user: {}};
      const response = await request(server).post(url).send(params);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
      expect(response.body.errors).toEqual({
        first_name: ['First Name is required'],
        last_name: ['Last Name is required'],
        email: ['Email is required'],
        password: ['Password is required'],
      });
      expect(response.body.message).toEqual(
          'First Name is required / ' +
          'Last Name is required / ' +
          'Email is required / ' +
          'Password is required',
      );
      done();
    });
  });

  describe('when parameters are present', () => {
    it('returns validation error for email format', async (done) => {
      const params = {
        user: {
          first_name: user.firstName,
          last_name: user.lastName,
          email: user.email+1,
          password: user.password,
          phone_number: faker.phone.phoneNumber('##########'),
        },
      };

      const response = await request(server).post(url).send(params);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body.errors).toEqual({
        email: ['Email format is invalid'],
      });
      expect(response.body.message).toEqual('Email format is invalid');
      done();
    });

    it('returns validation error for email already taken', async (done) => {
      const params = {
        user: {
          first_name: user.firstName,
          last_name: user.lastName,
          email: user.email,
          password: user.password,
          phone_number: faker.phone.phoneNumber('##########'),
        },
      };

      const response = await request(server).post(url).send(params);

      expect(response.unprocessableEntity).toBe(true);
      expect(response.body.errors).toEqual({
        email: ['email must be unique'],
      });
      expect(response.body.message).toEqual('email must be unique');
      done();
    });

    it('successfully creates the record', async (done) => {
      const params = {
        user: {
          first_name: user.firstName,
          last_name: user.lastName,
          email: faker.internet.email(),
          password: user.password,
          phone_number: faker.phone.phoneNumber('##########'),
        },
      };

      const response = await request(server).post(url).send(params);

      expect(response.created).toBe(true);
      expect(response.body).toMatchSchema(jsonTypes.userSchema());

      const latestUser = await utilities.latestRecord('User');
      expect(response.body).toEqual(jsonTypes.user(latestUser));
      done();
    });
  });
});
