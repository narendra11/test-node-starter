const errorsSchema = () => {
  return {
    properties: {
      message: {type: 'string'},
      errors: {type: 'object'},
    },
    required: ['message', 'errors'],
  };
};

const arraySchema = (innerSchema) => {
  return {type: 'array', items: innerSchema};
};

// user
const userSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      first_name: {type: 'string'},
      last_name: {type: 'string'},
      email: {type: 'string'},
      phone_number: {type: 'string'},
      role: {type: 'string'},
      status: {type: 'string'},
      slug: {type: 'string'},
    },
    required: ['id', 'first_name', 'last_name', 'email', 'phone_number', 'role', 'status', 'slug'],
  };
};

const userIndexSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      name: {type: 'string'},
    },
    required: ['id', 'name'],
  };
};

const user = (user) => {
  return {
    id: user.id,
    first_name: user.firstName,
    last_name: user.lastName,
    email: user.email,
    phone_number: user.phoneNumber,
    role: user.role,
    status: user.status,
    slug: user.slug,
  };
};

// tag
const tagSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      name: {type: 'string'},
    },
    required: ['id', 'name'],
  };
};

const tag = (tag) => {
  return {
    id: tag.id,
    name: tag.name,
  };
};

// tagging
const taggingSchema = () => {
  return {
    properties: {
      tag: tagSchema(),
      tagging: {type: 'object'},
    },
    required: ['tag', 'tagging'],
  };
};

// session
const sessionSchema = () => {
  return {
    auth_token: {
      properties: {
        body: {type: 'string'},
      },
      required: ['body'],
    },
    user: userSchema(),
  };
};

const session = (session) => {
  return {
    auth_token: session.body,
    user: user(session.user),
  };
};

// installation
const installationSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      user_id: {type: 'string'},
      token: {type: 'string'},
    },
    required: ['id', 'user_id', 'token'],
  };
};

const installation = (installation) => {
  return {
    id: installation.id,
    user_id: installation.userId,
    token: installation.token,
  };
};

// comment
const commentSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      user_id: {type: 'string'},
      commentable_id: {type: 'string'},
      commentable_type: {type: 'string'},
      body: {type: 'string'},
    },
    required: ['id', 'user_id', 'commentable_id', 'commentable_type', 'body'],
  };
};

const comment = (comment) => {
  return {
    id: comment.id,
    user_id: comment.userId,
    commentable_id: comment.commentableId,
    commentable_type: comment.commentableType,
    body: comment.body,
  };
};

// contact
const contactSchema = () => {
  return {
    properties: {
      message: {type: 'string'},
    },
    required: ['message'],
  };
};

// revision
const revisionSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      model: {type: 'string'},
      document: {type: 'object'},
      operation: {type: 'string'},
      document_id: {type: 'string'},
      revision: {type: 'integer'},
    },
    required: ['id', 'model', 'operation', 'document_id', 'revision'],
  };
};

// activity
const activitySchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      user_id: {type: 'string'},
      subject_id: {type: 'string'},
      subject_type: {type: 'string'},
      action: {type: 'string'},
    },
    required: ['id', 'user_id', 'subject_id', 'subject_type', 'action'],
  };
};

const activity = (activity) => {
  return {
    id: activity.id,
    user_id: activity.user_id,
    subject_id: activity.subject_id,
    subject_type: activity.subject_type,
    action: activity.action,
  };
};

// notification
const notificationSchema = () => {
  return {
    properties: {
      id: {type: 'string'},
      title: {type: 'string'},
      description: {type: 'string'},
      status: {type: 'string'},
    },
    required: ['id', 'title', 'description', 'status'],
  };
};

const notification = (notification) => {
  return {
    id: notification.id,
    activity: activity(notification.activity),
    title: notification.title,
    description: notification.description,
    status: notification.status,
  };
};


module.exports = {
  errorsSchema,
  arraySchema,
  userSchema, userIndexSchema, user,
  tagSchema, tag,
  taggingSchema,
  sessionSchema, session,
  installationSchema, installation,
  commentSchema, comment,
  contactSchema,
  revisionSchema,
  activitySchema,
  activity,
  notificationSchema,
  notification,
};
