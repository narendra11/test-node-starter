const attachmentView = require('~views/attachments/attachment');
const attachmentIndexView = (attachments) => {
  return attachments.map((attachment) => attachmentView(attachment));
};

module.exports = attachmentIndexView;
