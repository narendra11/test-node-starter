const attachmentView = require('~views/attachments/attachment');
const attachmentIndexView = require('~views/attachments/index');
const authenticationTokenView = require('~views/authenticationTokens/authenticationToken');
const commentView = require('~views/comments/comment');
const commentsIndexView = require('~views/comments/index');
const installationView = require('~views/installations/installation');
const notificationView = require('~views/notifications/notification');
const notificationsIndexView = require('~views/notifications/index');
const taggingView = require('~views/taggings/tagging');
const taggingIndexView = require('~views/taggings/index');
const tagView = require('~views/tags/tag');
const tagIndexView = require('~views/tags/index');
const userView = require('~views/users/user');
const userIndexView = require('~views/users/index');

module.exports = {
  attachmentView,
  attachmentIndexView,
  authenticationTokenView,
  commentView,
  commentsIndexView,
  installationView,
  notificationView,
  notificationsIndexView,
  taggingView,
  taggingIndexView,
  tagView,
  tagIndexView,
  userView,
  userIndexView,
};

