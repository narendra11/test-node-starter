const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {jsonTypes, unauthenticated, utilities} = require('~testHelper');

describe('API::V1::UsersController#current', () => {
  const url = '/api/v1/users/current_user';

  describe('when user is authenticated', () => {
    describe('when admin is logged in', () => {
      it('returns admin details', async (done) => {
        const adminToken = await utilities.token({role: 'admin'});
        const response = await request(server).get(url).set(...adminToken.header);

        expect(response.ok).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.userSchema());
        expect(response.body).toEqual(jsonTypes.user(adminToken.authenticationToken.user));
        done();
      });
    });

    describe('when user is logged in', () => {
      it('returns user details', async (done) => {
        const userToken = await utilities.token();
        const response = await request(server).get(url).set(...userToken.header);

        expect(response.ok).toBe(true);
        expect(response.body).toMatchSchema(jsonTypes.userSchema());
        expect(response.body).toEqual(jsonTypes.user(userToken.authenticationToken.user));
        done();
      });
    });
  });

  unauthenticated(url);
});
