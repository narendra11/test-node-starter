const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const server = require('~server');
const {InstallationFactory, TagFactory} = require('~factories');
const {jsonTypes, unauthenticated, utilities, unauthorized} = require('~testHelper');

describe('API::V1::TaggingsController#create', () => {
  let installation;
  let tag;
  let params;
  const url = '/api/v1/taggings';

  beforeAll(async () => {
    installation = await InstallationFactory.create();
    tag = await TagFactory.create();
    params = {
      tagging: {
        taggableType: 'Installation',
        taggableId: installation.id,
        tagId: tag.id,
      },
    };
  });

  describe('when user is authenticated', () => {
    describe('when user is authorized', () => {
      describe('and parameters are present', () => {
        it('if user is authorized to create the tagging', async (done) => {
          const userToken = await utilities.token({user: await installation.getUser()});
          const response = await request(server).post(url).set(...userToken.header).send(params);
          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.taggingSchema());
          done();
        });

        it('admin is authorized to create the tagging', async (done) => {
          const adminToken = await utilities.token({role: 'admin'});
          const response = await request(server).post(url).set(...adminToken.header).send(params);
          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.taggingSchema());
          done();
        });
      });
    });

    describe('when user is not authorized', () => {
      it('gives permission error', async (done) => {
        const userToken = await utilities.token();
        unauthorized({url: url, verb: 'post', token: userToken, params: params});
        done();
      });
    });
  });

  unauthenticated(url, 'post');
});
