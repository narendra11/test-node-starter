const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const {User, AuthenticationToken} = require('~models');
const {AuthenticationTokenFactory} = require('~factories');

describe('AuthenticationToken', () => {
  let authenticationToken;
  let latestAuthenticationToken;

  beforeAll(async () => {
    authenticationToken = await AuthenticationTokenFactory.create();
    latestAuthenticationToken = await AuthenticationToken.findOne({
      order: [['createdAt', 'DESC']],
    });
  });

  it('is saved in database', async (done) => {
    expect(latestAuthenticationToken.id).toEqual(authenticationToken.id);
    expect(latestAuthenticationToken.isNewRecord).toBe(false);
    done();
  });

  it('generates valid uuid', async (done) => {
    expect(authenticationToken.id.length).toEqual(36);
    done();
  });

  it('has given attributes', async (done) => {
    expect(authenticationToken).toHaveProperty('body');
    done();
  });

  describe('associations', () => {
    test('user', async (done) => {
      expect(await authenticationToken.getUser()).toBeInstanceOf(User);
      done();
    });
  });

  describe('default properties', () => {
    it('validates expiresIn', async (done) => {
      expect(authenticationToken.expiresIn).toEqual(3600);
      done();
    });
  });

  describe('validations', () => {
    it('validates body requirement', async (done) => {
      try {
        await AuthenticationToken.create({
          body: '',
          userId: authenticationToken.userId,
          ipAddress: authenticationToken.ipAddress,
          userAgent: authenticationToken.userAgent,
          expiresIn: authenticationToken.expiresIn,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Body is required']);
      }
      done();
    });

    it('validates user id requirement', async (done) => {
      try {
        await AuthenticationToken.create({
          body: authenticationToken.body,
          userId: '',
          ipAddress: authenticationToken.ipAddress,
          userAgent: authenticationToken.userAgent,
          expiresIn: authenticationToken.expiresIn,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User id is required']);
      }
      done();
    });

    it('validates ip address requirement', async (done) => {
      try {
        await AuthenticationToken.create({
          body: authenticationToken.body,
          userId: authenticationToken.userId,
          ipAddress: '',
          userAgent: authenticationToken.userAgent,
          expiresIn: authenticationToken.expiresIn,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['IP address is required']);
      }
      done();
    });

    it('validates user agent requirement', async (done) => {
      try {
        await AuthenticationToken.create({
          body: authenticationToken.body,
          userId: authenticationToken.userId,
          ipAddress: authenticationToken.ipAddress,
          userAgent: '',
          expiresIn: authenticationToken.expiresIn,
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['User agent is required']);
      }
      done();
    });

    it('validates expires in requirement', async (done) => {
      try {
        await AuthenticationToken.create({
          body: authenticationToken.body,
          userId: authenticationToken.userId,
          ipAddress: authenticationToken.ipAddress,
          userAgent: authenticationToken.userAgent,
          expiresIn: '',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeValidationError');
        expect(error.errors.length).toEqual(1);
        expect(error.errors.map((err) => err.message)).toEqual(['Expires in is required']);
      }
      done();
    });

    it('validates expires in string', async (done) => {
      try {
        await AuthenticationToken.create({
          body: authenticationToken.body,
          userId: authenticationToken.userId,
          ipAddress: authenticationToken.ipAddress,
          userAgent: authenticationToken.userAgent,
          expiresIn: 'invalid time',
        });
      } catch (error) {
        expect(error.name).toBe('SequelizeDatabaseError');
        expect(error.message).toEqual('invalid input syntax for integer: "invalid time"');
      }
      done();
    });
  });

  describe('methods', () => {
    test('verifyAuthToken', async (done) => {
      expect(await AuthenticationToken.verifyAuthToken({
        user_id: authenticationToken.userId, body: authenticationToken.body,
      })).toBe(true);
      done();
    });
  });
});
