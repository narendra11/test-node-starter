'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Tagging extends Model {
    static associate(models) {
      // define association here
      models.Tagging.belongsTo(models.Tag, {
        foreignKey: 'tagId',
        constraints: false,
        onDelete: 'CASCADE',
        as: 'tag',
      });
    }
  };
  Tagging.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    tagId: {
      type: DataTypes.UUID,
      unique: 'tt_unique_constraint',
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Tag id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Tag id is required',
        },
      },
    },
    taggableId: {
      type: DataTypes.UUID,
      unique: 'tt_unique_constraint',
      references: null,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Taggable id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Taggable id is required',
        },
      },
    },
    taggableType: {
      type: DataTypes.STRING,
      unique: 'tt_unique_constraint',
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Taggable type is required',
        },
        notEmpty: {
          args: true,
          msg: 'Taggable type is required',
        },
      },
    },
  }, {
    sequelize,
    modelName: 'Tagging',
  });

  return Tagging;
};
