const Controller = require('~controllers/v1/controller');

const {UserMessage} = require('~helpers/constants');
const tokenManager = require('~libs/tokenManager');

module.exports = class PasswordController extends Controller {
  static async forgotPassword(request, response, next) {
    const user = await Controller.finders.findUser(request.body.user.email);

    user.generateResetPasswordToken(user);

    return response.status(Controller.HttpStatus.OK)
        .json({message: UserMessage.FORGOT_PASSWORD_MSG});
  }

  static async resetPassword(request, response, next) {
    await tokenManager.decode(request.body.user.resetPasswordToken);
    const user = await Controller.finders.findResetPassword(request.body.user.resetPasswordToken);

    await user.update({
      password: request.body.user.password,
      reset_password_token: null,
      reset_password_sent_at: null,
    });

    return response.status(Controller.HttpStatus.OK)
        .json({message: UserMessage.PASSWORD_UPDATED});
  }
};
