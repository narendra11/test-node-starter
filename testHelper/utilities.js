const {AuthenticationTokenFactory} = require('~factories');
const models = require('~models');
const {User} = require('~models');

const token = async (options = {}) => {
  const authenticationToken = await AuthenticationTokenFactory.create(options);

  return {
    authenticationToken: authenticationToken,
    header: ['Authorization', `Bearer ${authenticationToken.body}`],
  };
};

const latestRecord = async (type) => {
  return await models[type].findOne({
    order: [['createdAt', 'DESC']],
  });
};

const latestAuthRecord = async (type) => {
  return await models[type].findOne({
    order: [['createdAt', 'DESC']],
    include: {
      model: User,
      as: 'user',
    },
  });
};

module.exports = {
  token, latestRecord, latestAuthRecord,
};
