const request = require('supertest');
const {matchers} = require('jest-json-schema');
expect.extend(matchers);

const faker = require('faker');

const server = require('~server');
const {jsonTypes, utilities, unauthenticated} = require('~testHelper');
const {User} = require('~models');

describe('API::V1::UsersController#update', () => {
  let userToken;
  const url = '/api/v1/users';

  beforeAll(async () => {
    userToken = await utilities.token();
  });

  describe('when user is authenticated', () => {
    describe('update profile', () => {
      describe('when parameters are missing', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              first_name: '',
              last_name: '',
              email: '',
              phone_number: '',
            },
          };

          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          const firstNameError = 'First Name is required';
          const lastNameError = 'Last Name is required';
          const emailFormatError = 'Email format is invalid';
          const emailError = 'Email is required';

          expect(response.body.errors).toEqual({
            first_name: [firstNameError],
            last_name: [lastNameError],
            email: [emailFormatError, emailError],
          });

          const nameCombineError = `${firstNameError} / ${lastNameError}`;
          const emailCombineError = `${emailFormatError} / ${emailError}`;
          expect(response.body.message).toEqual(`${nameCombineError} / ${emailCombineError}`);
          done();
        });
      });

      describe('when parameters are present', () => {
        it('updates the profile', async (done) => {
          const userUpdate = await User.build({
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: faker.internet.email(),
            phoneNumber: faker.phone.phoneNumber('##########'),
          });

          const params = {
            user: {
              first_name: userUpdate.firstName,
              last_name: userUpdate.lastName,
              email: userUpdate.email,
              phone_number: userUpdate.phoneNumber,
            },
          };

          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.userSchema());
          expect(response.body.id).toEqual(userToken.authenticationToken.user.id);

          const latestUser = await utilities.latestRecord('User');

          expect(response.body).toEqual(jsonTypes.user(latestUser));
          expect(response.body.first_name).toEqual(userUpdate.firstName);
          expect(response.body.last_name).toEqual(userUpdate.lastName);
          expect(response.body.email).toEqual(userUpdate.email);
          expect(response.body.phone_number).toEqual(userUpdate.phoneNumber);
          done();
        });
      });
    });

    describe('change password', () => {
      describe('when current password is blank', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: '',
              password: 'New Password',
              password_confirmation: 'New Password',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            current_password: ['Current Password is required'],
          });
          expect(response.body.message).toEqual('Current Password is required');
          done();
        });
      });

      describe('when current password is incorrect', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: 'invalid password',
              password: 'New Password',
              password_confirmation: 'New Password',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unauthorized).toBe(true);
          expect(response.body.message).toEqual('Invalid current password');
          done();
        });
      });

      describe('when password confirmation is not sent', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: userToken.authenticationToken.user.password,
              password: 'New Password',
              password_confirmation: '',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            password_confirmation: ['Password Confirmation is required'],
          });
          expect(response.body.message).toEqual('Password Confirmation is required');
          done();
        });
      });

      describe('when password confirmation does not match', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: userToken.authenticationToken.user.password,
              password: 'New Password',
              password_confirmation: 'New Password does not match',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            password_confirmation: ['The Password Confirmation and Password fields must match.'],
          });
          expect(response.body.message)
              .toEqual('The Password Confirmation and Password fields must match.');
          done();
        });
      });

      describe('when new password is not sent', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: userToken.authenticationToken.user.password,
              password: '',
              password_confirmation: 'New Password does not match',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            password: ['Password is required'],
            password_confirmation: ['The Password Confirmation and Password fields must match.'],
          });
          expect(response.body.message)
              .toEqual('Password is required / '+
                    'The Password Confirmation and Password fields must match.');
          done();
        });
      });

      describe('when new password is shorter than 6 characters', () => {
        it('returns validation errors', async (done) => {
          const params = {
            user: {
              current_password: userToken.authenticationToken.user.password,
              password: 'New',
              password_confirmation: 'New',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.unprocessableEntity).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.errorsSchema());
          expect(response.body.errors).toEqual({
            password: ['The Password must be at least 6 characters.'],
            password_confirmation: ['The Password Confirmation must be at least 6 characters.'],
          });
          expect(response.body.message)
              .toEqual('The Password must be at least 6 characters. / ' +
                      'The Password Confirmation must be at least 6 characters.');
          done();
        });
      });

      describe('when parameters are present', () => {
        it('updates the password', async (done) => {
          const params = {
            user: {
              first_name: userToken.authenticationToken.user.firstName + 'new',
              last_name: userToken.authenticationToken.user.lastName + 'new',
              current_password: userToken.authenticationToken.user.password,
              password: 'New Password',
              password_confirmation: 'New Password',
            },
          };
          const response = await request(server).put(url).set(...userToken.header).send(params);

          expect(response.ok).toBe(true);
          expect(response.body).toMatchSchema(jsonTypes.userSchema());
          expect(response.body.id).toEqual(userToken.authenticationToken.user.id);

          const latestUser = await utilities.latestRecord('User');
          expect(response.body).toEqual(jsonTypes.user(latestUser));

          // login check with new credentials
          const loginUrl = '/api/v1/users/sign_in';
          const loginParams = {
            user: {
              email: response.body.email,
              password: 'New Password',
            },
          };
          const loginResponse = await request(server).post(loginUrl)
              .set('User-Agent', faker.internet.userAgent())
              .send(loginParams);

          expect(loginResponse.created).toBe(true);
          expect(loginResponse.body).toMatchSchema(jsonTypes.sessionSchema());
          const latestToken = await utilities.latestAuthRecord('AuthenticationToken');
          expect(loginResponse.body).toEqual(jsonTypes.session(latestToken));

          done();
        });
      });
    });
  });

  unauthenticated(url, 'put');
});
