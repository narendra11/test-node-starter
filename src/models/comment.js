'use strict';
const {v4: uuidv4} = require('uuid');
const {Model} = require('sequelize');
const _ = require('lodash');

module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    static associate(models) {
      // define association here
      models.Comment.belongsTo(models.User, {
        foreignKey: 'userId',
        as: 'user',
      });

      models.Comment.belongsTo(models.Installation, {
        foreignKey: 'commentableId',
        onDelete: 'CASCADE',
        constraints: false,
        as: 'installation',
      });

      models.Comment.hasMany(models.Activity, {
        foreignKey: 'subjectId',
        constraints: false,
        scope: {
          subjectType: 'comment',
        },
        onDelete: 'CASCADE',
        as: 'activities',
      });
    }

    async getCommentable(options) {
      if (!this.commentableType) return Promise.resolve(null);
      const mixinMethodName = `get${_.startCase(this.commentableType)}`;
      return await this[mixinMethodName](options);
    }

    async parent() {
      return await this.getCommentable();
    }

    description() {
      return this.body;
    }

    async watchers() {
      const creator = await this.getUser();
      return [creator];
    }
  };
  Comment.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      allowNull: false,
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'User id is required',
        },
        notEmpty: {
          args: true,
          msg: 'User id is required',
        },
      },
    },
    commentableId: {
      type: DataTypes.UUID,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Commentable id is required',
        },
        notEmpty: {
          args: true,
          msg: 'Commentable id is required',
        },
      },
    },
    commentableType: {
      type: DataTypes.STRING,
      allowNull: false,
      //   defaultValue: 'user',
      validate: {
        notNull: {
          args: true,
          msg: 'Commentable type is required',
        },
        notEmpty: {
          args: true,
          msg: 'Commentable type is required',
        },
        // isIn: {
        //   args: [['user', 'installation']],
        //   msg: 'Commentable Type not found',
        // },
      },
    },
    body: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notNull: {
          args: true,
          msg: 'Body is required',
        },
        notEmpty: {
          args: true,
          msg: 'Body is required',
        },
      },
    },
  }, {
    hooks: {
      afterCreate: async (comment, options) => {
        await comment.createActivity({
          userId: options.user.id,
          action: 'added',
        });
      },
    },
    sequelize,
    modelName: 'Comment',
    defaultScope: {
      order: [['createdAt', 'DESC']],
    },
  });

  return Comment;
};
