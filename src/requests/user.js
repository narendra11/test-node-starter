const Validator = require('~requests/base');
const {HttpStatus} = require('~helpers/constants');
const utils = require('~helpers/utils');

const register = async (req, res, next) => {
  const validationRule = {
    'first_name': 'required|string',
    'last_name': 'required|string',
    'email': 'required|string|email|unique:User,email',
    'phone_number': 'string',
    'password': 'required|string|min:6',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.user, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const update = async (req, res, next) => {
  let validationRule = {
    'first_name': 'string',
    'last_name': 'string',
    'email': 'string|email',
    'phone_number': 'string',
  };

  if (('password' in req.body.user)) {
    validationRule = {
      'currentPassword': 'required|string',
      'password': 'required|string|min:6',
      'passwordConfirmation': 'required|string|min:6|same:password',
    };
  }

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.user, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const signIn = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
    'password': 'required|string',
  };

  const message = {
    required: ':attribute is required',
  };

  await Validator(req.body.user, validationRule, message, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const emailAvailable = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
  };
  await Validator(req.query, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const forgotPassword = async (req, res, next) => {
  const validationRule = {
    'email': 'required|string|email',
  };
  await Validator(req.body.user, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

const resetPassword = async (req, res, next) => {
  const validationRule = {
    'password': 'required|string|min:6',
    'passwordConfirmation': 'required|same:password',
    'resetPasswordToken': 'required',
  };
  await Validator(req.body.user, validationRule, {}, (err, status) => {
    if (!status) {
      res.status(HttpStatus.UNPROCESSABLE_ENTITY)
          .send(utils.errorResponse(err.errors));
    } else {
      next();
    }
  });
};

module.exports = {
  register, signIn, emailAvailable, forgotPassword, resetPassword, update,
};
